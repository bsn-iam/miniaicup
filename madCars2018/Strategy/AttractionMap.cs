﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public class AttractionMap : BonusMap
    {
        public AttractionMap(Range2 activeWorldRange, MapType mapType, int mapPointsAmount) : base (activeWorldRange, MapType.Final, mapPointsAmount) {

        }

        //public override void RegenerateMap(WorldState worldState)
        //{
        //    InitTable();
        //    var universe = Strategy.Universe;
        //    var tickIndex = universe.TickIndex;
        //    var predictedEnemy = universe.Predictor.GetStateOnTick(tickIndex + 1, tickIndex).Units.First(u => u.Id == 0);
        //    var predictedEnemyCenter = new Point(predictedEnemy.X, predictedEnemy.Y);
        //    var predictedPositionShift = predictedEnemyCenter - universe.EnemyCar.CenterPosition;
        //
        //    var predictedEnemyButton =  universe.EnemyCar.GetAbsolutePosition(universe.Constants.Button.First()) + predictedPositionShift;
        //
        //    AddUnitCalculation(predictedEnemyButton, 10, 1, 1200);
        //}

        public override double CalculateRewardInWorldPoint(Point point, WorldState worldState)
        {
            var universe = Strategy.Universe;
            var tickIndex = universe.TickIndex;
            var predictedEnemy = universe.Predictor.GetStateOnTick(tickIndex + 1, tickIndex).Units.First(u => u.Id == 0);
            var predictedEnemyCenter = new Point(predictedEnemy.X, predictedEnemy.Y);
            var predictedPositionShift = predictedEnemyCenter - universe.EnemyCar.CenterPosition;
            var predictedEnemyButton = universe.EnemyCar.GetAbsolutePosition(universe.Constants.Button.First()) + predictedPositionShift;

            var reward = AddUnitPointCalculation(predictedEnemyButton, point, 10, 1, 1200, 2);
            return reward;
        }
    }
}
