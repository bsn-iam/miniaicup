﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;

namespace Strategy.Visualizer
{
    public static partial class Visualizer
    {

        public static void CreateForm()
        {
            if (_form == null)
            {
                _thread = new Thread(_showWindow);
                _thread.Start();
                Thread.Sleep(2000);
            }
        }

        private static void _showWindow()
        {
            try
            {
                _form = new MainForm();
                _form.ShowDialog();
                _form.Focus();
            }
            catch (Exception showDialogException)
            {
                //Strategy.Universe.Print(showDialogException.Message + showDialogException.StackTrace + showDialogException.InnerException);
            }

        }

        public static MainForm _form;
        private static Thread _thread;
        private static Graphics _graphics;
        public static Stopwatch VisualTimer = new Stopwatch();

        private delegate void DrawDelegate();

        public static void Draw()
        {

            VisualTimer.Start();
            //visualTimer.Reset();
            if (_form.InvokeRequired)
            {
                _form.BeginInvoke(new DrawDelegate(Draw), new object[] { });
                return;
            }

            Done = false;
            if (Visualizer._form.renderCheckBox.Checked)
                _draw(Strategy.Universe);
            SegmentsDrawQueue.Clear();
            Done = true;
            VisualTimer.Stop();

        }

        public static void CheckPauseState()
        {
            while (Visualizer.Pause && !Visualizer.RenderPressed)
            {
                // pause here
            }
            if (Visualizer.RenderPressed)
                Visualizer.RenderPressed = false;
        }

        private static bool isXYStartOnTop;
        private static double correctAxis(this double value) => isXYStartOnTop ? value : WorldHeight - value;
        private static int correctAxis(this int value) => isXYStartOnTop ? value : (int)WorldHeight - value;

        private static void DrawBonusMap(BonusMap requiredMap)
        {
            var tileGenerator = new TileGenerator();
            var tileList = tileGenerator.GetTileList(requiredMap);

            foreach (var tile in tileList)
                if (Math.Abs(tile.Value) > Double.Epsilon)
                {
                    var color01 = new Color01(1, 1 - tile.Value, 1 - tile.Value);
                    FillRect(color01.ToColor(), tile.CenterPosition.X, tile.CenterPosition.Y, tile.Size * 1, tile.Size * 1);

                    if (tile.Value > 0.9999)
                        FillRect(Color.YellowGreen, tile.CenterPosition.X, tile.CenterPosition.Y, tile.Size * 1, tile.Size * 1);

                    if (Math.Abs(tile.RealValue) > Double.Epsilon)
                        DrawText($"{tile.RealValue:f2}", 3, Brushes.Black, tile.CenterPosition.X, tile.CenterPosition.Y);
                }
        }

        private static void DrawCircle(Color color, Point p, double radius, float width = 0)
        {
            DrawCircle(color, p.X, p.Y, radius, width);
        }
        private static void DrawCircle(Color color, double x, double y, double radius, float width = 0)
        {
            var pen = width > 0 ? new Pen(color, width) : new Pen(color);
            _graphics.DrawEllipse(pen, _X(x - radius), _Y(y.correctAxis() - radius), _S(radius * 2), _S(radius * 2));
        }

        private static void FillCircle(Color color, double x, double y, double radius)
        {
            _graphics.FillEllipse(new SolidBrush(color), _X(x - radius), _Y(y.correctAxis() - radius), _S(radius * 2), _S(radius * 2));
        }

        private static void FillCircle(Color color, Point p, double radius)
        {
            _graphics.FillEllipse(new SolidBrush(color), _X(p.X - radius), _Y(p.Y.correctAxis() - radius), _S(radius * 2), _S(radius * 2));
        }

        private static void FillRect(Color color, double x, double y, double w, double h)
        {
            _graphics.FillRectangle(new SolidBrush(color), _X(x), _Y(y.correctAxis()), _S(w), _S(h));
        }

        private static void DrawLine(Color color, double x, double y, double X, double Y, float width = 0F)
        {
            _graphics.DrawLine(new Pen(color, width), _X(x), _Y(y.correctAxis()), _X(X), _Y(Y.correctAxis()));
        }
        private static void DrawLine(Color color, Point p1, Point p2, float width = 0F)
        {
            _graphics.DrawLine(new Pen(color, width), _X(p1.X), _Y(p1.Y.correctAxis()), _X(p2.X), _Y(p2.Y.correctAxis()));
        }

        //private static void DrawPie(Color color, double x, double y, double r, double startAngle, double endAngle, float width = 0F)
        //{
        //    startAngle = Geometry.ToDegrees(startAngle);
        //    endAngle = Geometry.ToDegrees(endAngle);
        //    _graphics.DrawPie(new Pen(color), new Rectangle(_X(x - r), _Y(y.correctAxis() - r), _S(2 * r), _S(2 * r)), (float)startAngle, (float)(endAngle - startAngle));
        //}

        private static void DrawText(string text, double size, Brush brush, double x, double y)
        {
            var font = new Font("Comic Sans MS", _S(size));
            _graphics.DrawString(text, font, brush, _X(x), _Y(y.correctAxis()));
        }

        private static double _lookX = 0, _lookY = 0, _scale = 1;

        private static int _X(double x)
        {
            return (int)((x - _lookX) / _scale);
        }

        private static int _Y(double y)
        {
            return (int)((y - _lookY) / _scale);
        }

        private static int _S(double x)
        {
            return (int)Math.Ceiling(x / _scale);
        }

        public static List<object[]> SegmentsDrawQueue = new List<object[]>();
        public static List<Tuple<System.Drawing.Point, double>> DangerPoints;
        public static Dictionary<long, System.Drawing.Point[]>
            Projectiles = new Dictionary<long, System.Drawing.Point[]>();

        public class Color01
        {
            public double R, G, B;

            public Color01(double r, double g, double b)
            {
                R = r;
                G = g;
                B = b;
            }

            public Color ToColor()
            {
                return Color.FromArgb((int)(255 * R), (int)(255 * G), (int)(255 * B));
            }
        }

        private static Color01 _grad2(Color01 col1, Color01 col2, double x)
        {
            return new Color01(
                (col2.R - col1.R) * x + col1.R,
                (col2.G - col1.G) * x + col1.G,
                (col2.B - col1.B) * x + col1.B
            );
        }

        public static Color01[] BadColors = new[] {
            new Color01(0x8B / 255.0, 0, 0),// red!!
            new Color01(1, 0, 0),// red
            new Color01(1, 69 / 255.0, 0),// orange
            new Color01(1, 1, 0),// yellow
            new Color01(1, 1, 1),// white
        };

        public static Color01[] GoodColors = new[] {
            new Color01(1, 1, 1),// white
            new Color01(0, 1, 0),// green
        };

        //        public static Color01 Gradient(Color01[] colors, double x)
        //        {
        //            var delta = 1.0 / (colors.Length - 1);
        //            for (var i = 0; i < colors.Length - 1; i++)
        //            {
        //                var left = delta*i;
        //                var right = delta * (i + 1);
        //                if (left <= x && x <= right)
        //                {
        //                    return _grad2(colors[i], colors[i + 1], (x - left) * (colors.Length - 1));
        //                }
        //            }
        //            //throw new Exception("wrong x ranges");
        //        }

        public static int DrawSince { get; set; } = 0;

        public static bool Done;


        public static double Zoom
        {
            get
            {
                return _scale;
            }
            set
            {
                if (value > 0)
                    _scale = value;
            }
        }

        public static bool RenderPressed { get; internal set; }

        public static bool Pause = false;

        public static void LookAt(Point point, double worldWidth, double worldHeight, double scale = -1)
        {
            Zoom = scale;

            _lookY = point.Y.correctAxis() - _scale * _form.panel.Height / 2;
            if (_lookY < 0)
                _lookY = 0;
            if (_lookY > worldHeight - _scale * _form.panel.Height)
                _lookY = worldHeight - _scale * _form.panel.Height;

            _lookX = point.X - _scale * _form.panel.Width / 2;
            if (_lookX < 0)
                _lookX = 0;
            if (_lookX > worldWidth - _scale * _form.panel.Width)
                _lookX = worldWidth - _scale * _form.panel.Width;

        }

    }
}
