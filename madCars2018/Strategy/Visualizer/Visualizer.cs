﻿//using MyStrategy;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;

namespace Strategy.Visualizer
{
    public static partial class Visualizer
    {
        public static Universe Universe;
        public static List<long> StepTimeList = new List<long>();
        private static double WorldHeight = Strategy.Constants.GameHeight;
        private static double WorldWidth = Strategy.Constants.GameWidth;

        public static void _draw(Universe universe)
        {
            try
            {
                #region Configuration

                Universe = universe;
                var panel = _form.panel;

                isXYStartOnTop = false;

                var bonusMapId = 0;
                var ballNo = 0;
                int.TryParse(_form.mapIdTextBox.Text.Trim(), out bonusMapId);
                int.TryParse(_form.lookAtTextBox.Text.Trim(), out ballNo);

                var lookAtObject = Universe.MyCar;
                if (lookAtObject != null)
                    LookAt(new Point(lookAtObject.Position), WorldWidth, WorldHeight);

                var drawArea = new Bitmap((int)WorldWidth, (int)WorldHeight);
                panel.Image = drawArea;
                _graphics = Graphics.FromImage(drawArea);

                StepTimeList.Add(Universe.MainTimer.ElapsedMilliseconds);
                if (StepTimeList.Count > 1000) StepTimeList.RemoveAt(0);

                _form.MainStepTime.Text = $"{StepTimeList.Average():f2} ms";
                _form.VisualStepTime.Text = $"{VisualTimer.ElapsedMilliseconds / 1000:f1} ms";
                _form.TickIndex.Text = $"{Universe.TickIndex}";

                #endregion

                #region BonusMap

                if (lookAtObject != null && lookAtObject.BMCalculator.BonusMapList.Any())
                {
                    BonusMap requiredMap;
                    if (bonusMapId >= lookAtObject.BMCalculator.BonusMapList.Count)
                        requiredMap = lookAtObject.BMCalculator.ResultingMap;
                    else
                        requiredMap = lookAtObject.BMCalculator.BonusMapList[bonusMapId];

                    if (requiredMap != null)
                        DrawBonusMap(requiredMap);
                }
                #endregion

                //mapGeometry
                foreach (var s in Universe.Constants.Map)
                    DrawLine(Color.Brown, s.A.X, s.A.Y, s.B.X, s.B.Y, 10);

                //Cars
                //DrawCar(lookAtObject, Color.Green);
                //DrawCar(universe.EnemyCar, Color.Black);

                //Route
                if (lookAtObject.Route != null)
                    foreach (var p in lookAtObject.Route.PointsWorld)
                        FillCircle(Color.DarkBlue, p, 2);

                //DeadLine
                DrawLine(Color.Red, 0, universe.DeadLinePosition, WorldWidth, universe.DeadLinePosition, 5);

                //Score
                //for (var i = 1; i <= universe.Constants.MyLives; i++)
                //    FillCircle(Color.Green, 50 * i, WorldHeight - 50, 20);
                //
                //for (var i = 1; i <= universe.Constants.EnemyLives; i++)
                //    FillCircle(Color.DarkRed, 50 * i, WorldHeight - 100, 20);

                //Tree
                var tree = Strategy.Tree;
                if (tree != null)
                {
                    foreach (var line in tree.Lines.Values)
                        DrawLineForBranchSequence(Color.Blue, tree, line);
                    DrawLineForBranchSequence(Color.Green, tree, tree.BestLine, 3);
                }


                #region System

                // map ranges
                DrawLine(Color.Black, 1, 1, 1, WorldHeight - 1, 3);
                DrawLine(Color.Black, 1, WorldHeight - 1, WorldWidth - 1, WorldHeight - 1, 3);
                DrawLine(Color.Black, WorldWidth - 1, 1, WorldWidth - 1, WorldHeight - 1, 3);
                DrawLine(Color.Black, WorldWidth - 1, 1, 1, 1, 3);

                for (var i = 0; i < WorldHeight; i = i + 100)
                    DrawLine(Color.LightSlateGray, 0, i, WorldWidth, i);
                for (var i = 0; i < WorldWidth; i = i + 100)
                    DrawLine(Color.LightGray, i, 0, i, WorldHeight);


                foreach (var seg in SegmentsDrawQueue)
                {
                    var points = seg[0] as List<System.Drawing.Point>;
                    var pen = seg[1] as Pen;
                    float width = seg.Length > 2 ? Convert.ToSingle(seg[2]) : 0F;
                    for (var i = 1; i < points.Count; i++)
                        DrawLine(pen.Color, points[i].X, points[i].Y, points[i - 1].X, points[i - 1].Y, width);
                }
                #endregion

            }
            catch { }

        }

        private static void DrawLineForBranchSequence(Color color, Tree tree, TreeLine line, int width = 0)
        {
            foreach (var branchId in line.BranchIDs)
            {
                var branch = tree.BranchList[branchId];
                var lineEndColor = branch.IsLineEnd &&
                    branch.Id != tree.BestLine.BranchIDs.Last() ? 
                    Color.Gray : color;
                //foreach (var point in branch.Route)
                //    DrawCircle(lineEndColor, point, 2, 1/4);

                DrawLine(lineEndColor, branch.Route.First(), branch.Route.Last(), width);
            }
        }

        private static void DrawCar(Car car, Color color)
        {
            //Shape
            var p1 = Universe.Constants.CarForm.First();
            Point pen = car.GetAbsolutePosition(p1);
            foreach (var p in Universe.Constants.CarForm)
            {
                var absPos = car.GetAbsolutePosition(p);
                DrawLine(color, pen, absPos, 5);
                pen = absPos;
            }
            var penLast = car.GetAbsolutePosition(p1);
            DrawLine(color, pen, penLast, 5);

            //Button
            pen = car.GetAbsolutePosition(Universe.Constants.Button.First());
            foreach (var p in Universe.Constants.Button)
            {
                var absPos = car.GetAbsolutePosition(p);
                DrawLine(Color.BlueViolet, pen, absPos, 3);
                pen = absPos;
            }
            penLast = car.GetAbsolutePosition(Universe.Constants.Button.First());
            DrawLine(Color.BlueViolet, pen, penLast, 3);

            var wheels = Universe.Constants.Wheels;
            FillCircle(Color.Cornsilk, car.GetAbsolutePosition(wheels.RearWheelPosition), wheels.RearWheelRadius);
            FillCircle(Color.Cornsilk, car.GetAbsolutePosition(wheels.FrontWheelPosition), wheels.FrontWheelRadius);

            //Centers of axis
            FillCircle(Color.Violet, car.Position.X, car.Position.Y, 2);
            FillCircle(Color.DarkOrange, car.CenterPosition.X, car.CenterPosition.Y, 2);
        }

    }


}
