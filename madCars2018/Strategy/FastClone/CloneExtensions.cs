﻿using FastClone;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Strategy.FastClone
{
    public static class CloneExtensions
    {
        public static T DeepCloneStream<T>(this T toClone)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, toClone);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        public static T DeepCloneExpTree<T>(this T toClone)
        {
            return Cloner.Clone(toClone);
        }
    }
}
