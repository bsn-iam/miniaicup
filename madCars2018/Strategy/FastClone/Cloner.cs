﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using FastClone.Internal;

namespace FastClone
{
    public static class Cloner
    {
        static readonly ConcurrentDictionary<Type, Func<object, Dictionary<object, object>, object>> _TypeCloners = new ConcurrentDictionary<Type, Func<object, Dictionary<object, object>, object>>();

        /// <summary>
        /// Creates a deep clone.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="original"></param>
        /// <returns></returns>
        public static T Clone<T>(T original)
        {
            Func<object, Dictionary<object, object>, object> creator = GetTypeCloner(typeof(T));
            return (T)creator(original, new Dictionary<object, object>());
        }

        static Func<object, Dictionary<object, object>, object> GetTypeCloner(Type type) { return _TypeCloners.GetOrAdd(type, t => new CloneExpressionBuilder(t).CreateTypeCloner()); }
    }
}