﻿using System.Collections.Generic;

namespace Strategy
{
    public class Constants
    {

        public double GameWidth { get; set; } = 1200;
        public double GameHeight { get; set; } = 800;
        public int MapPointsAmount { get; } = 60;
        public int MapTickIndex { get; internal set; } = 0;

        public List<Segment> Map { get; set; } = new List<Segment>();
        public List<Point> CarForm { get; internal set; } = new List<Point>();
        public List<Point> Button { get; internal set; } = new List<Point>();
        public int MapId { get; internal set; }
        public Wheels Wheels { get; internal set; }

        public int CarId { get; internal set; } = 0;
        public int MyLives { get; internal set; } = 5;
        public int EnemyLives { get; internal set; } = 5;
    }
}
