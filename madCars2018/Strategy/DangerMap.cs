﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public class DangerMap : BonusMap
    {
        public DangerMap(Range2 activeWorldRange, MapType mapType, int mapPointsAmount) : base (activeWorldRange, MapType.Final, mapPointsAmount) {
        }

        public override bool IsValid(WorldState worldState)
        {
            var universe = Strategy.Universe;
            var tickIndex = universe.TickIndex;
            var predictedEnemyCenter = universe.Predictor.GetStateOnTick(tickIndex + 1, tickIndex).Units.First(u => u.Id == 0);
            //var enemyCenter = universe.EnemyCar.CenterPosition;
            var myCenter = universe.MyCar.CenterPosition;

            if (predictedEnemyCenter.Y > myCenter.Y)
                if (Math.Abs(myCenter.X - predictedEnemyCenter.X) < 200)
                    return true;
            return false;

        }

        //public override void RegenerateMap(WorldState worldState)
        //{
        //    InitTable();
        //    var universe = Strategy.Universe;
        //    var tickIndex = universe.TickIndex;
        //    var predictedEnemy = universe.Predictor.GetStateOnTick(tickIndex + 1, tickIndex).Units.First(u => u.Id == 0);
        //    var enemyCenter = new Point(predictedEnemy.X, predictedEnemy.Y);
        //
        //    AddUnitCalculation(enemyCenter, 0, 1, 180);
        //}

        public override double CalculateRewardInWorldPoint(Point point, WorldState worldState)
        {
            var universe = Strategy.Universe;
            var tickIndex = universe.TickIndex;
            var predictedEnemy = universe.Predictor.GetStateOnTick(tickIndex + 1, tickIndex).Units.First(u => u.Id == 0);
            var enemyCenter = new Point(predictedEnemy.X, predictedEnemy.Y);

            var reward = AddUnitPointCalculation(enemyCenter, point, 0, 1, 180, 2);
            return reward;
        }
    }
}
