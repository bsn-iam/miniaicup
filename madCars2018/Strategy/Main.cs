﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Strategy
{
    public class Strategy
    {
        public static Constants Constants=new Constants();
        public static Stopwatch MainTimer = new Stopwatch();
        public static int TickIndex = 0;
        public static Universe Universe;
        public static Car MyCar = new Car();
        public static Car EnemyCar = new Car();
        public static double DeadLinePosition;
        public static LinearPredictor Predictor = new LinearPredictor();
        public static Tree Tree;

        private static int visualSkippedCounter = 0;

        static void Main(string[] args)
        {
            //Console.ReadLine();


            while (true)
            {
#if DEBUG
                Debugger.Launch();
                //var trig = new Trigonometry();
                Visualizer.Visualizer.CreateForm();
#endif
                MainTimer.Restart();
                TickIndex += 1;

                var x = Console.ReadLine();
                if (String.IsNullOrEmpty(x)) break;
                var parsed = JObject.Parse(x);
                var tickType = (String)parsed.GetValue("type");
                var parameters = (JObject)parsed.GetValue("params");
                if (tickType == "new_match")
                {
                    SetupScene(parameters);
                }
                else
                {
                    RunTick(parameters);
                }

            }
        }

        private static void RunTick(JObject parameters)
        {
            var myCarData = (JArray)parameters.GetValue("my_car");
            var enemyCarData = (JArray)parameters.GetValue("enemy_car");
            var deadLineData = (JValue)parameters.GetValue("deadline_position");
            DeadLinePosition = (double)deadLineData;
            GetConstants("");
            if (myCarData == null)
                throw new Exception("Empty car data");

            Universe = new Universe(MyCar, EnemyCar, MainTimer, TickIndex, Constants, DeadLinePosition, Predictor);

            UpdateCar(MyCar, myCarData, true);
            UpdateCar(EnemyCar, enemyCarData, false);

            Predictor.RunTick(TickIndex, new List<Car> { MyCar, EnemyCar });

            Universe = new Universe(MyCar, EnemyCar, MainTimer, TickIndex, Constants, DeadLinePosition, Predictor);
            var worldState = new WorldState(MyCar, EnemyCar, TickIndex, Constants, DeadLinePosition, Predictor);

            Tree = new Tree(worldState, MyCar.BMCalculator, 20, 5000, 10, 1);
            Tree.Generate();

            var command = GenerateCommand(worldState);

            SendCommand(command);

            MainTimer.Stop();

#if DEBUG
            var strategyTime = MainTimer.ElapsedMilliseconds;
            var visualTime = Visualizer.Visualizer.VisualTimer.ElapsedMilliseconds;
            var stepsToSkip = Math.Floor((decimal)visualTime / strategyTime) - 1;

            if (stepsToSkip > 1 
                && visualSkippedCounter < stepsToSkip 
                && strategyTime < 100)
                visualSkippedCounter++;
            else
            {
                visualSkippedCounter = 0;
                Visualizer.Visualizer.Draw();
            }
            Visualizer.Visualizer.CheckPauseState();
#endif
        }


        private static void SetupScene(JObject parameters)
        {
            var myLives = (JValue)parameters.GetValue("my_lives");
            var enemyLives = (JValue)parameters.GetValue("enemy_lives");
            Constants.MyLives = (int)myLives;
            Constants.EnemyLives = (int)enemyLives;

            var protoCar = (JObject)parameters.GetValue("proto_car");
            var protoCarPoly = (JArray)protoCar.GetValue("car_body_poly");
            Constants.CarForm = new List<Point>();
            foreach (var pair in protoCarPoly)
                Constants.CarForm.Add(new Point((double)pair[0], (double)pair[1]));

            Constants.Wheels = new Wheels(protoCar);
            Constants.CarId = (int)(JValue)protoCar.GetValue("external_id");

            var buttonPoly = (JArray)protoCar.GetValue("button_poly");
            Constants.Button = new List<Point>();
            foreach (var pair in buttonPoly)
                Constants.Button.Add(new Point((double)pair[0], (double)pair[1]));

            var protoMap = (JObject)parameters.GetValue("proto_map");
            var protoMapId = (JValue)protoMap.GetValue("external_id");
            Constants.MapId = (int)protoMapId;
            Constants.MapTickIndex = TickIndex;

            var protoMapSegments = (JArray)protoMap.GetValue("segments");
            Constants.Map = new List<Segment>();
            foreach (var s  in protoMapSegments)
            {
                var p1 = s[0].ToObject<double[]>();
                var p2 = s[1].ToObject<double[]>();
                Constants.Map.Add(new Segment(new Point(p1[0], p1[1]), new Point(p2[0], p2[1])));
            }

            MyCar.InitiateMaps();

            Predictor = new LinearPredictor();

        }

        private static Car UpdateCar(Car car, JArray data, bool isMyCar)
        {
            car.Angle = new Angle((double)data[1]);
            var coords = data[0].ToObject<double[]>();
            car.Position = new Point(coords[0], coords[1]);
            car.StartSide = (int)data[2] == 1 ? Side.Left : Side.Right;

            car.UpdateInternalData();
            car.IsMine = isMyCar ? 1 : 0;
            return car;
        }

        private static Angle aggressiveAngleOrder = Angle.FromDegrees(60.0);

        private static Commands GenerateCommand(WorldState worldState)
        {
            if (TickIndex < 5)
                return Commands.Stop;

            var myCar = Universe.MyCar;

            myCar.IntegrateMaps(worldState);
            //Commands command;
            var angleDeg = MyCar.Angle.ValueInDegrees;
            var panicState = isInPanic();

            // Sorry, no fields this time
            var safeRange = new Range(-60, 60);
            if (safeRange.Contains(angleDeg))
            {
                myCar.IntegrateMaps(worldState);
                myCar.CalcluateBestDestination(100);
                var positionOrder = myCar.Route.NextWorldPoint;
                return GoToPosition(positionOrder);
            }

            //rise the nose!
            if (panicState && DeadLinePosition < 150)
            {
                var angleOrder = new Angle();
                if (isNotABus()) //Not bus
                    angleOrder = Angle.FromDegrees(25);
                else angleOrder = Angle.FromDegrees(60);
                return KeepAngle(angleOrder);
            }

            //Climb higher
            if (panicState && DeadLinePosition > 150)
                if (MyCar.X > EnemyCar.X)
                    return isNotABus() ? Commands.Right : Commands.Left;
                else return isNotABus() ? Commands.Left : Commands.Right;

            //Jump to rack
            if (Constants.MapId == 4 && !isNotABus() )
                return TryToJump(myCar);

            return KeepAngle(aggressiveAngleOrder);
        }

        private static bool isNotABus()
        {
            return Constants.CarId != 2;
        }

        private static Commands TryToJump(Car myCar)
        {
            var upperPositionRange = new Range2(300, 900, 400, 800);

            if (TickIndex < Constants.MapTickIndex + 50) //40
            {
                return Commands.Stop;
            }
            else
            {
                if (TickIndex < Constants.MapTickIndex + 200)
                {
                    if (myCar.CenterPosition.IsInRange(upperPositionRange))
                        return KeepAngle(Angle.FromDegrees(0)); //90
                    return GoBackWard();
                }
                if (myCar.CenterPosition.IsInRange(upperPositionRange))
                    return Commands.Stop;
                return KeepAngle(aggressiveAngleOrder);
            }
        }

        private static Commands GoBackWard()
        {
            if (MyCar.StartSide == Side.Left)
                return Commands.Left;
            return Commands.Right;
        }

        private static Commands GoToPosition(Point positionOrder)
        {
            Commands command;
            if (MyCar.CenterPosition.X > positionOrder.X)
                command = Commands.Left;
            else
                command = Commands.Right;
            return command;
        }

        private static Commands KeepAngle(Angle angleOrder)
        {
            Commands command;
            if (MyCar.StartSide == Side.Left)
            {
                if (MyCar.Angle.ValueInDegrees < angleOrder.ValueInDegrees )//&& Math.Abs(MyCar.Angle.ValueInDegrees) < 175)
                    command = Commands.Right;
                else
                    command = Commands.Left;
            }
            else
            {
                if (MyCar.Angle.ValueInDegrees < -angleOrder.ValueInDegrees )//&& Math.Abs(MyCar.Angle.ValueInDegrees) < 175)
                    command = Commands.Right;
                else
                    command = Commands.Left;
            }

            return command;
        }

        private static bool isInPanic()
        {
            var myWinHeight = MyCar.WinHeight;
            var enemyWinHeight = EnemyCar.WinHeight;

            if (DeadLinePosition > 11)
                if (MyCar.WinHeight < EnemyCar.WinHeight)
                    return true;
            return false;
        }

        private static void SendCommand (Commands command)
        {
            switch (command)
            {
                case  Commands.Left:
                    Console.WriteLine("{\"command\":\"left\"}");
                    break;
                case Commands.Right:
                    Console.WriteLine("{\"command\":\"right\"}");
                    break;
                case Commands.Stop:
                    Console.WriteLine("{\"command\":\"stop\"}");
                    break;
            }
        }

        public enum Commands
        {
            Right,
            Left,
            Stop
        }
        private static void GetConstants(string configData)
        {
           // var config = JObject.Parse(configData);
            //Constants = new Constants()
            //{
            //    GameWidth = 1200,
            //    GameHeight = 800,
            //    //GameWidth = (double)config["GAME_WIDTH"],
            //    //GameHeight = (double)config["GAME_HEIGHT"],
            //};
        }
    }
}
