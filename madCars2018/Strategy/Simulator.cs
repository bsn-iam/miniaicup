﻿using Strategy.FastClone;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public class Simulator : SimulatorBase
    {
        private BonusMapCalculator BMCalculator;
        private Range2 GameRange;
        public Simulator(BonusMapCalculator bmCalculator)
        {
            BMCalculator = bmCalculator;
            GameRange = new Range2(0, Strategy.Constants.GameWidth - 1, 0, Strategy.Constants.GameHeight - 1);
        }
        public override void IntegrateAndGetReward(Branch branch, int tickLength)
        {
            var unit = branch.InitialState.MyCar;
            var calculatedPosition = new Point(unit.X, unit.Y);
            branch.Route.Add(calculatedPosition);
            var order = branch.Order.Vector;

            var currentWorldState = branch.InitialState;

            for (var tick=1; tick <= tickLength; tick++)
            {
                calculatedPosition += order;
                if (!calculatedPosition.IsInRange(GameRange))
                {
                    branch.Reward -= 100;
                    break;
                }
               
                branch.Route.Add(calculatedPosition);
                //currentWorldState.DeepCloneExpTree();
                currentWorldState = currentWorldState.Clone();
                currentWorldState.MyCar.Position = calculatedPosition.Clone();
                currentWorldState.MyCar.X = calculatedPosition.X;
                currentWorldState.MyCar.Y = calculatedPosition.Y;

                branch.Reward += BMCalculator.CalculateReward(calculatedPosition, currentWorldState);
            }
            branch.FinalState = currentWorldState;
        }


        public void Clear()
        {
            //StateList = new Dictionary<int, WorldState>()
        }

       
    }
}
