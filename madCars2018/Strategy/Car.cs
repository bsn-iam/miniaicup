﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Strategy
{
    public class Car : IPredictable
    {
        public Point Position { get; set; }
        public Angle Angle { get; set; }
        public BonusMapCalculator BMCalculator;
        public Side StartSide { get; internal set; } = Side.Right;
        public Point CenterPosition { get; internal set; } = new Point();
        public double IsMine { get; internal set; } = 0;
        public double WinHeight = 0;
        public Point BestDestination;
        public BonusMapRoute Route;

        private Range2 WorldRange = new Range2(0, Strategy.Constants.GameWidth, 0, Strategy.Constants.GameHeight);
        private int maxWorldStepPerTick = 100;

        public double X
        {
            get
            {
                return CenterPosition.X;
            }

            set
            {
                CenterPosition = new Point(value, CenterPosition.Y);
            }
        }

        public double Y
        {
            get
            {
                return CenterPosition.Y;
            }

            set
            {
                CenterPosition = new Point(CenterPosition.X, value);
            }
        }

        double IPredictable.Id
        {
            get
            {
                return IsMine;
            }

            set
            {
                IsMine = value;
            }
        }

        public void InitiateMaps()
        {
            BMCalculator = new BonusMapCalculator();

            //var attractionMap = new AttractionMap(WorldRange, MapType.Flat, Strategy.Constants.MapPointsAmount);
            //attractionMap.SetIsDynamic(true).SetWeight(1).SetName("Attraction");
            //BMCalculator.AddMap(attractionMap);

            //var dangerMap = new DangerMap(WorldRange, MapType.Flat, Strategy.Constants.MapPointsAmount);
            //dangerMap.SetIsDynamic(true).SetWeight(-8.5).SetName("Danger");
            //BMCalculator.AddMap(dangerMap);

            var buildingsMap = new BuildingsMap(WorldRange, MapType.Flat, Strategy.Constants.MapPointsAmount);
            buildingsMap.SetIsDynamic(false).SetWeight(2).SetName("Buildings");
            BMCalculator.AddMap(buildingsMap);
        }

        public void IntegrateMaps(WorldState worldState)
        {
            BMCalculator.SetActiveRange(CenterPosition, maxWorldStepPerTick, 1, 7);
            BMCalculator.RegenerateMapList(worldState);
            BMCalculator.ResultingMap.RegenerateMap(worldState);
        }

        public void UpdateInternalData()
        {
            CenterPosition = GetAbsolutePosition(new Point(90, 20));

            var absoluteButtonPoints = new List<Point>();
            foreach (var buttonPoint in Strategy.Universe.Constants.Button)
                absoluteButtonPoints.Add(GetAbsolutePosition(buttonPoint));
            WinHeight = absoluteButtonPoints.Select(p => p.Y).Min();
        }

        public void CalcluateBestDestination(int maxLength)
        {
            Route = new BonusMapRoute(BMCalculator.ResultingMap, CenterPosition, maxLength);
            Route.CalculateWorldDestination(maxWorldStepPerTick);
        }

        public Point GetAbsolutePosition(Point relativePosition)
        {
            var mult = StartSide == Side.Left ? 1 : -1;
            var pRotated = relativePosition.RotateCCW(mult * Angle.ValueInSI);
            return new Point(Position.X + pRotated.X * mult, Position.Y + pRotated.Y);
        }

        public IPredictable Clone()
        {
            var clone = new Car();
            clone.Position = Position;
            clone.IsMine = IsMine;

            return clone;
        }
        public Car LazyClone()
        {
            var clone = new Car();
            clone.Position = Position.Clone();
            clone.IsMine = IsMine;

            return clone;
        }
    }

    public enum Side
    {
        Right,
        Left
    }

    public class Wheels
    {
        public Point FrontWheelPosition { get; private set; }
        public double FrontWheelRadius { get; private set; }
        public Point RearWheelPosition { get; private set; }
        public double RearWheelRadius { get; private set; }
        public Wheels(JObject protoCar)
        {
            FrontWheelRadius = (double)(JValue)protoCar.GetValue("front_wheel_radius");
            var FWPos = (JArray)protoCar.GetValue("front_wheel_position");
            FrontWheelPosition = new Point((double)FWPos[0], (double)FWPos[1]);

            RearWheelRadius = (double)(JValue)protoCar.GetValue("rear_wheel_radius");
            var RWPos = (JArray)protoCar.GetValue("rear_wheel_position");
            RearWheelPosition = new Point((double)RWPos[0], (double)RWPos[1]);
        }

    }


}
