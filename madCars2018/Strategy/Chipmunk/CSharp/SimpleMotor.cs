using System;
using System.Runtime.InteropServices;
using Strategy.Lib.CApi;

namespace Strategy.Lib.CSharp {
    public class SimpleMotor : Constraint {
        public double Rate {
            get {
                return CP.SimpleMotorGetRate(Handle);
            }
            set {
                CP.SimpleMotorSetRate(Handle, value);
            }
        }

        public SimpleMotor(Body a, Body b, double rate) {
            Handle = CP.SimpleMotorNew(a.Handle, b.Handle, rate);
        }
    }
}
