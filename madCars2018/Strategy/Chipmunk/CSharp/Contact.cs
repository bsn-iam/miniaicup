﻿using Strategy.Lib.CApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Strategy.Lib.CSharp {
    public class Contact {
        public double Depth { get; set; }
        public Vect PointA { get; set; }
        public Vect PointB { get; set; }
    }
}
