﻿using Strategy.Lib.CApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy.Lib.CSharp {
    public class KinematicBody : Body {
        public KinematicBody() : base(CP.BodyNewKinematic()) {
        }
    }
}
