﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategy.Lib.CApi;

namespace Strategy.Lib.CSharp
{
    public static class Utils
    {
        public static double MomentForSegment(double mass, Vect a, Vect b, double radius)
        {
            return CP.MomentForSegment(mass, a, b, radius);
        }
        public static double MomentForCircle(double mass, double r1, double r2, Vect offset)
        {
            return CP.MomentForCircle(mass, r1, r2, offset);
        }

    }
}
