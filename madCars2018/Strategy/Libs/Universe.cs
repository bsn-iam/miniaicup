﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Strategy
{
    public class Universe
    {
        public Stopwatch MainTimer { get; private set; }
        public int TickIndex { get; private set; }
        public Car MyCar { get; private set; }
        public Car EnemyCar { get; private set; }
        public Constants Constants { get; private set; }
        public double DeadLinePosition { get; private set; }

        public LinearPredictor Predictor { get; private set; }

        public Universe(Car myCar, Car enemyCar, Stopwatch mainTimer, int tickIndex, Constants constants, double deadLinePosition, LinearPredictor predictor)
        {
            this.MainTimer = mainTimer;
            this.TickIndex = tickIndex;
            this.MyCar = myCar;
            this.EnemyCar = enemyCar;
            this.Constants = constants;
            this.DeadLinePosition = deadLinePosition;
            this.Predictor = predictor;
        }
    }

}