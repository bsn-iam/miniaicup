﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;


namespace Strategy
{
    public class LinearPredictor
    {
        public SortedList<int, LinearPredictorState> StateList { get; internal set; } = new SortedList<int, LinearPredictorState>();

        internal void RunTick(int currentTickIndex, IEnumerable<IPredictable> units)
        {
            //clear all predictions
            foreach (var state in new SortedList<int, LinearPredictorState>(StateList))
                if (!state.Value.IsRealValue)
                    StateList.Remove(state.Key);

            //clear all old reals state values
            foreach (var state in new SortedList<int, LinearPredictorState> (StateList))
                if (state.Key < currentTickIndex - 5)
                    StateList.Remove(state.Key);

            StateList.Add(currentTickIndex, new LinearPredictorState(new List<IPredictable>(units), true));

        }

        public LinearPredictorState GetStateOnTick(int tick, int currentTickIndex)
        {
            foreach (var state in StateList)
                if (state.Key == tick)
                    return StateList[tick];

            StateList.Add(tick, IntegrateTillTick(tick, currentTickIndex));
            return StateList[tick];
        }

        private LinearPredictorState IntegrateTillTick(int tick, int currentTickIndex)
        {
            var timer = new Stopwatch();
            timer.Start();
            var predictedOppUnits = new List<IPredictable>();

            var allRealUnits = StateList[currentTickIndex].Units; // If !Any()  -- RunTick() was not called.

            foreach (var unit in allRealUnits)
            {
                var unitSpeed = CalculateUnitSpeed(unit, currentTickIndex);

                var predictedX = unit.X + unitSpeed.SpeedX * (tick - currentTickIndex);
                var predictedY = unit.Y + unitSpeed.SpeedY * (tick - currentTickIndex);

                var predictedUnit = unit.Clone();
                predictedUnit.X = predictedX;
                predictedUnit.Y = predictedY;

                predictedOppUnits.Add(predictedUnit);
            }
            timer.Stop();
            return new LinearPredictorState(predictedOppUnits, false).SetTime(timer.ElapsedMilliseconds);
        }

        private Speed CalculateUnitSpeed(IPredictable unit, int currentTickIndex)
        {
            var unitSpeed = new Speed(0, 0);
            if (StateList.LastOrDefault().Value == null)
                return unitSpeed;
            
            var stateOld = StateList.LastOrDefault(s => s.Value.IsRealValue && s.Key < currentTickIndex - 2);

            if (stateOld.Value == null)
                return unitSpeed;

            var unitOld = stateOld.Value.Units.FirstOrDefault(u => u.Id.Equals(unit.Id));

            if (unitOld == null)
                return unitSpeed;

            var ticksBetweenStates = currentTickIndex - stateOld.Key;

            if (ticksBetweenStates > 0)
                unitSpeed = new Speed((unit.X - unitOld.X) / ticksBetweenStates,
                    (unit.Y - unitOld.Y) / ticksBetweenStates);
            else
                unitSpeed = new Speed(0, 0);

            return unitSpeed;
        }
    }

    public class Speed
    {
        public Speed(double speedX, double speedY)
        {
            this.SpeedX = speedX;
            this.SpeedY = speedY;
        }

        public double SpeedX { get; set; }
        public double SpeedY { get; set; }
    }

    public interface IPredictable
    {
        double X { get; set; }
        double Y { get; set; }
        double Id { get; set; }

        IPredictable Clone();
    }
}
