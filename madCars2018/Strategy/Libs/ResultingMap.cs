﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public class ResultingMap : BonusMap
    {
        private List<BonusMap> BonusMapList = new List<BonusMap>();

        public ResultingMap(List<BonusMap> bonusMapList) : base(bonusMapList.First(), MapType.Final)
        {
            this.BonusMapList = bonusMapList;
        }


        public override void RegenerateMap(WorldState worldState)
        {
            InitTable();
            ActiveMapRange = GetMinActiveRange();

            foreach (var map in BonusMapList.Where(t=>t.Table!=null))
            {
                map.Trim();

                if (map.IsEmpty)
                    continue;
                if (!map.IsValid(worldState))
                    continue;

                if (XMapSize != map.XMapSize)
                    throw new Exception("Different map resolution");

                //int mapResolutionKoeff = (int) Math.Round((double) map.XMapSize / XMapSize);
                var iMin = map.ActiveMapRange.XMin;
                var iMax = map.ActiveMapRange.XMax;
                var jMin = map.ActiveMapRange.YMin;
                var jMax = map.ActiveMapRange.YMax;

                for (int i = iMin; i < iMax; i++)
                    for (int j = jMin; j < jMax; j++)
                        Table[i, j] += map.Table[i, j] * map.Weight;
            }

            //CheckTileGeneration(bonusMapList, 8); //Debug only
            Trim();
        }

        private Range2Int GetMinActiveRange()
        {
            //Get max Active range
            int xMin = ActiveMapRange.XMin;
            int xMax = ActiveMapRange.XMax;
            int yMin = ActiveMapRange.YMin;
            int yMax = ActiveMapRange.YMax;
            foreach (var map in BonusMapList)
            {
                if (map.ActiveMapRange.XMin > xMin)
                    xMin = map.ActiveMapRange.XMin;
                if (map.ActiveMapRange.XMax < xMax)
                    xMax = map.ActiveMapRange.XMax;
                if (map.ActiveMapRange.YMin > yMin)
                    yMin = map.ActiveMapRange.YMin;
                if (map.ActiveMapRange.YMax < yMax)
                    yMax = map.ActiveMapRange.YMax;
            }
            return new Range2Int(xMin, xMax, yMin, yMax);
        }
    }
}
