﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Strategy
{
    public abstract class BonusMap
    {
        public double[,] Table;

        public double[,] RealTable { get; internal set; }

        public MapType MapType { get; }
        public double Weight { get; set; }
        public bool IsEmpty { get; internal set; }
        public int XMapSize { get; private set; }

        public int YMapSize { get; private set; }
        public int XWorldSize { get; } = (int)Strategy.Constants.GameWidth;
        public int YWorldSize { get; } = (int)Strategy.Constants.GameHeight;
        public double MapCellWidth { get; private set; }
        public Range2Int ActiveMapRange { get; protected set; }
        public Range2 ActiveWorldRange { get; private set; }

        internal void UpdateRangeFromTemplate (BonusMap templateMap)
        {
            ActiveMapRange = templateMap.ActiveMapRange;
            XMapSize = templateMap.XMapSize;
            YMapSize = XMapSize * YWorldSize / XWorldSize;
            MapCellWidth = (double)XWorldSize / XMapSize; // > 1
        }

        public long MapTime { get; private set; }
        public string Name { get; private set; }
        public bool IsDynamic { get; internal set; } = false;

        public BonusMap(Range2 activeWorldRange, MapType mapType, int mapPointsAmount)
        {
            MapType = mapType;

            XMapSize = mapPointsAmount;
            YMapSize = XMapSize * YWorldSize / XWorldSize;
            MapCellWidth = (double)XWorldSize / XMapSize; // > 1

            SetActiveWorldRange(activeWorldRange);
            SetActiveMapRange(activeWorldRange);

            CheckActiveMapRange();
        }

        private void CheckActiveMapRange()
        {
            if (ActiveMapRange.XMin > XMapSize ||
                ActiveMapRange.XMax > XMapSize ||
                ActiveMapRange.YMin > YMapSize ||
                ActiveMapRange.YMax > YMapSize)
                throw new Exception("Wrong ActiveRange values!");
        }

        private void SetActiveMapRange(Range2 activeWorldRange)
        {
            ActiveMapRange = new Range2Int((int)Math.Round(activeWorldRange.XMin / MapCellWidth),
                                            (int)Math.Round(activeWorldRange.XMax / MapCellWidth),
                                            (int)Math.Round(activeWorldRange.YMin / MapCellWidth),
                                            (int)Math.Round(activeWorldRange.YMax / MapCellWidth));
        }

        private void SetActiveWorldRange(Range2 activeWorldRange)
        {
            ActiveWorldRange = activeWorldRange; //.Clone();
            //ActiveWorldRange = new Range2(activeWorldRange.XMin,
            //                               activeWorldRange.XMax,
            //                               activeWorldRange.YMin,
            //                               activeWorldRange.YMax);
        }

        public BonusMap(BonusMap templateMap, MapType mapType)
        {
            MapType = mapType;

            XMapSize = templateMap.XMapSize;
            YMapSize = XMapSize * YWorldSize / XWorldSize;
            MapCellWidth = (double)XWorldSize / XMapSize; // > 1

            SetActiveMapRange(templateMap);
            SetActiveWorldRange(templateMap.ActiveWorldRange);
        }

        private void SetActiveMapRange(BonusMap templateMap)
        {
            ActiveMapRange = new Range2Int(
                templateMap.ActiveMapRange.XMin,
                templateMap.ActiveMapRange.XMax,
                templateMap.ActiveMapRange.YMin,
                templateMap.ActiveMapRange.YMax);
        }

        public void InitTable()
        {
            Table = new double[XMapSize + 1, YMapSize + 1];
        }

        internal void ResetActiveRange()
        {
            ActiveMapRange = new Range2Int(0, XMapSize, 0, YMapSize);
        }

        public BonusMap Trim()
        {
            double maxValue = Double.MinValue;
            double minValue = Double.MaxValue;

            //find max value of the map
            var iMin = ActiveMapRange.XMin;
            var iMax = ActiveMapRange.XMax;
            var jMin = ActiveMapRange.YMin;
            var jMax = ActiveMapRange.YMax;

            for (int i = iMin; i < iMax; i++)
                for (int j = jMin; j < jMax; j++)
                { 
                    if (Table[i, j] > maxValue)
                        maxValue = Table[i, j];
                    if (Table[i, j] < minValue)
                        minValue = Table[i, j];
                }

            IsEmpty = Math.Abs(minValue - maxValue) < Double.Epsilon;

            if (IsEmpty)
                return this;

            //scale map to range [0, 1]
            for (int i = iMin; i < iMax; i++)
                for (int j = jMin; j < jMax; j++)
                    Table[i, j] = (Table[i, j] - minValue) / (maxValue - minValue);

            return this;
        }


        public void AddUnitCalculation(Point unitAbsolutePosition, double maxValueDistance, double maxValue, double zeroValueDistance)
        {
            if (maxValueDistance > zeroValueDistance)
            {
                throw new Exception("Wrong distance limits.");
                //zeroValueDistance = maxValueDistance;
            }

            var maxValueDistanceSquared = maxValueDistance * maxValueDistance;
            var zeroValueDistanceSquared = zeroValueDistance * zeroValueDistance;

            var power = 2;
            var maxValuePow = Math.Pow(maxValue, power);

            for (int i = ActiveMapRange.XMin; i < ActiveMapRange.XMax; i++)
                for (int j = ActiveMapRange.YMin; j < ActiveMapRange.YMax; j++)
                {
                    var distanceSquared = unitAbsolutePosition.GetSquaredDistanceTo(i * MapCellWidth, j * MapCellWidth);

                    if (distanceSquared <= maxValueDistanceSquared)
                    {
                        if (MapType == MapType.Flat)
                            Table[i, j] = Math.Max(maxValuePow, Table[i, j]);
                        else
                            Table[i, j] += maxValuePow;
                    }

                    if (distanceSquared > maxValueDistanceSquared && distanceSquared < zeroValueDistanceSquared)
                    {
                        var value = maxValue - ((distanceSquared - maxValueDistanceSquared) / zeroValueDistanceSquared);
                        var valuePow = Math.Pow(value, power);

                        if (MapType == MapType.Flat)
                            Table[i, j] = Math.Max(valuePow, Table[i, j]);
                        else
                            Table[i, j] += valuePow;
                    }
                }
        }

        public virtual void RegenerateMap(WorldState worldState)
        {
            InitTable();
            for (int i = ActiveMapRange.XMin; i < ActiveMapRange.XMax; i++)
                for (int j = ActiveMapRange.YMin; j < ActiveMapRange.YMax; j++)
                {
                    var worldRewardPoint = new Point(i * MapCellWidth, j * MapCellWidth);
                    var pointReward = CalculateRewardInWorldPoint(worldRewardPoint, worldState);
                    FillTableWithReward(pointReward, i, j);
                }
        }

        public virtual double CalculateRewardInWorldPoint(Point point, WorldState worldState)
        {
            //var reward = AddUnitPointCalculation(worldState.Enemy.Position, point, 10, 1, 500, 2);
            //return reward;
            return 0;
        }

        protected double AddUnitPointCalculation(Point unitAbsolutePosition, Point requestedRewardPoint, double maxValueDistance, double maxValue, double zeroValueDistance, double power)
        {
            if (maxValueDistance > zeroValueDistance)
                throw new Exception("Wrong distance limits.");

            double result = 0;
            var maxValuePow = Math.Pow(maxValue, power);
            var maxValueDistanceSquared = maxValueDistance * maxValueDistance;
            var zeroValueDistanceSquared = zeroValueDistance * zeroValueDistance;
            var distanceSquared = unitAbsolutePosition.GetSquaredDistanceTo(requestedRewardPoint.X, requestedRewardPoint.Y);

            if (distanceSquared <= maxValueDistanceSquared)
            {
                result = maxValuePow;
            } else
            {
                if (distanceSquared < zeroValueDistanceSquared)
                {
                    var value = maxValue - ((distanceSquared - maxValueDistanceSquared) / zeroValueDistanceSquared);
                    var valuePow = Math.Pow(value, power);
                    result = valuePow;
                }
                    
            }
            return result;
        }

        private void FillTableWithReward(double pointReward, int tableX, int tableY)
        {
            switch (MapType)
            {
                case MapType.Flat:
                {
                    Table[tableX, tableY] = Math.Max(pointReward, Table[tableX, tableY]);
                    break;
                }
                case MapType.Additive:
                {
                    Table[tableX, tableY] += pointReward;
                    break;
                }
                case MapType.Final:
                {
                    Table[tableX, tableY] += pointReward;
                    break;
                }
                default:
                {
                    throw new Exception("Not defined map behaviour");
                }
            }
        }

        public BonusMap SetWeight(double weigth)
        {
            Weight = weigth;
            return this;
        }

        public BonusMap SetIsDynamic(bool isDynamic)
        {
            IsDynamic = isDynamic;
            return this;
        }

        internal BonusMap SetName(string name)
        {
            Name = name;
            return this;
        }

        internal BonusMap SetTime(long elapsedMilliseconds)
        {
            MapTime = elapsedMilliseconds;
            return this;
        }

        public void SetRealTable()
        {
            RealTable = (double[,])Table.Clone();
        }

        public virtual bool IsOutdated(WorldState worldState) => true;
        public virtual bool IsValid(WorldState worldState) => true;

    }

    public enum MapType
    {
        Additive,
        Flat,
        Final,
    }


}

