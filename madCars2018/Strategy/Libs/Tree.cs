﻿using Strategy.FastClone;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Strategy
{
    public class Tree
    {
        public Dictionary<int, Branch> BranchList { get; private set; } = new Dictionary<int, Branch>();
        public Dictionary<int, TreeLine> Lines { get; private set; } = new Dictionary<int, TreeLine>();

        public double MaxTimeMsec { get; private set; }
        public int MaxLineLength { get; private set; }
        public int MaxBranchCount { get; private set; }
        public int BranchTickLength { get; private set; }
        public long GenerationTime { get; private set; }
        public Branch Root { get; private set; }
        public TreeLine BestLine { get; private set; }
        private Simulator Simulator;
        private CommandGenerator Command = new CommandGenerator();
        private int MaxUsedGuid = 0;

        public Tree (WorldState stateNow, BonusMapCalculator bmCalculator, int maxLineLength, int maxBranchCount, double maxTimeMsec, int branchTickLength)
        {
            this.MaxBranchCount = maxBranchCount;
            this.MaxLineLength = maxLineLength;
            this.MaxTimeMsec = maxTimeMsec;
            this.BranchTickLength = branchTickLength;
            this.Root = new Branch(stateNow, Command.ForceOrders.First(), GetNewGuid(), true);
            BranchList.Add(Root.Id, Root);
            Lines.Add(Root.Id, new TreeLine(Root));
            Simulator = new Simulator(bmCalculator);
            Simulator.IntegrateAndGetReward(Root, 0);
        }

        private void Fork (Branch parentBranch)
        {
            var parentLine = Lines[parentBranch.Id];

            foreach (var forceOrder in Command.ForceOrders)
            {
                var newId = GetNewGuid();
                var newBranch = new Branch(parentBranch.ForkState, forceOrder, newId);
                
                parentBranch.AddChild(newId);
                BranchList.Add(newId, newBranch);

                newBranch.SetParent(parentBranch.Id);
                Simulator.IntegrateAndGetReward(newBranch, BranchTickLength);

                var newLine = parentLine.Clone();
                newLine.Extend(newBranch);
                Lines.Add(newId, newLine);
            }
            Lines.Remove(parentBranch.Id);
        }

        //private void CheckTheDeepCopyPerformance(TreeLine currentLine)
        //{
        //    var timer = new Stopwatch();
        //    timer.Start();
        //
        //    var deepListExp = new List<TreeLine>();
        //    for (int i = 0; i < 10000; i++)
        //        deepListExp.Add(currentLine.DeepCloneExpTree());
        //    timer.Stop();
        //    var deepCopyExp = timer.ElapsedMilliseconds;
        //
        //    //timer.Restart();
        //    //var deepListStream = new List<TreeLine>();
        //    //for (int i = 0; i < 10000; i++)
        //    //    deepListStream.Add(currentLine.DeepCloneStream());
        //    //timer.Stop();
        //    var deepCopyStream = timer.ElapsedMilliseconds;
        //
        //    timer.Restart();
        //    var deepListManual = new List<TreeLine>();
        //    for (int i = 0; i < 10000; i++)
        //        deepListManual.Add(currentLine.Clone());
        //    timer.Stop();
        //    var deepCopyManual = timer.ElapsedMilliseconds;
        //
        //    var result = $"deepCopyExp = {deepCopyExp}, deepCopyManual = {deepCopyManual}, deepCopyStream = {deepCopyStream}";
        //}

        public void Generate()
        {
            var timer = new Stopwatch();
            timer.Restart();
            Simulator.Clear();

            while (BranchList.Count < MaxBranchCount
                && timer.ElapsedMilliseconds < MaxTimeMsec
                && GetCurrentMaxLineLength() < MaxLineLength)
            {
                var bestLine = ChooseBestLine();
                var branchForFork = BranchList[bestLine.BranchIDs.Last()];
                Fork(branchForFork);
            }

            BestLine = ChooseBestLine();
            GenerationTime = timer.ElapsedMilliseconds;

            var maxLineLength = GetCurrentMaxLineLength();
        }

        private int GetCurrentMaxLineLength()
        {
            int lineLength = 0;
            foreach (var line in Lines.Values)
            {
                var currentLineLength = line.LegsAmount;
                if (currentLineLength > lineLength)
                    lineLength = currentLineLength;
            }
            return lineLength;
        }

        private TreeLine ChooseBestLine()
        {
            double maxAverageReward = 0;
            var bestLine = Lines.Values.First();
            foreach (var line in Lines.Values)
            {
                var averageReward = line.GetLineReward();
                if (averageReward > maxAverageReward)
                {
                    maxAverageReward = averageReward;
                    bestLine = line;
                }
            }
            return bestLine;
        }

        private int GetNewGuid()
        {
            MaxUsedGuid++;
            return MaxUsedGuid;
        }

    }

    // 1. Sdelat' «vetvleniye» stvola tekushchego dereva (vsegda v seredine), 
    //yesli srednyaya dlina vetvey potomkov men'she dliny stvola.
    //Inache pereyti k vyboru potomka dlya rekursivnogo vyzova na nem. YA mnogo tut eksperimentiroval — eto variant okazalsya nailuchshim.

    // 2. Vybrat' potomka tekushchego dereva dlya rekursivnogo vyzova na nem. Vybirayetsya potomok s maksimal'nym znacheniyem: 
    //<srednyaya dlina vetvey v dereve> * <tret'ya otsenochnaya funktsiya> / sqrt(summa dlin vsekh vetvey dereva). 
    //Deleniye nuzhno chtoby derevo ne vyrozhdalos': chem bol'she vremeni potracheno na issledovaniye vetvi, tem bol'she znamenatel' i men'she prioritet.

    //3. Osnovnaya otsenochnaya funktsiya, na osnove kotoroy reshalos' po kakoy vetvi my vse zhe poyedem (kakaya vetv' luchshe). 
    //V pervoy versii znacheniye etoy funktsii ravnyalos' summe otsenki dlya stvola i maksimal'noy otsenki potomka.Nagrada davalas' 
    //za priblizheniye k finishu — «vzyatiye tayla». Pozzhe eta funktsiya sil'no izmenilas'.
}
