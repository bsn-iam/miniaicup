﻿namespace Strategy
{
    public abstract class SimulatorBase
    {
        public abstract void IntegrateAndGetReward(Branch branch, int tickLength);
    }
}