﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    [Serializable]
    public class WorldState
    {
        public int Id { get; }
        public bool IsRealValue { get; set; }
        public long CalculationTime { get; private set; }
        public int TickIndex { get; private set; }

        public Car MyCar { get; private set; }
        public Car EnemyCar { get; private set; }
        public Constants Constants { get; private set; }
        public double DeadLinePosition { get; private set; }

        public LinearPredictor Predictor { get; private set; }

        public WorldState(Car myCar, Car enemyCar, int tickIndex, Constants constants, double deadLinePosition, LinearPredictor predictor)
        {
            Id = Guid.NewGuid().GetHashCode();

            this.TickIndex = tickIndex;
            this.MyCar = myCar;
            this.EnemyCar = enemyCar;
            this.Constants = constants;
            this.DeadLinePosition = deadLinePosition;
            this.Predictor = predictor;
        }

        internal WorldState Clone()
        {
            return new WorldState(MyCar.LazyClone(), EnemyCar.LazyClone(), TickIndex, Constants, DeadLinePosition, Predictor);
        }
    }
}
