﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public class BonusMapRoute
    {
        public List<Point> PointsMap { get; private set; } = new List<Point>();
        public List<Point> PointsWorld { get; private set; } = new List<Point>();
        public double RouteSum { get; private set; } = 0;

        private List<double> UsedPoints = new List<double>();
        private Point StartPoint = new Point();
        public Point NextMapPoint { get; private set; } = new Point();
        public Point NextWorldPoint { get; private set; } = new Point();

        private BonusMap Map;

        public BonusMapRoute(BonusMap map, Point initialWorldPoint, int maxLength)
        {
            var mapCellWidth = map.MapCellWidth;
            var currentPoint = new Point(Math.Round(initialWorldPoint.X / mapCellWidth), Math.Round(initialWorldPoint.Y / mapCellWidth));
            PointsMap.Add(currentPoint);
            StartPoint = currentPoint.Clone();
            UsedPoints.Add(currentPoint.Hash);
            Map = map;

            int counter = 0;
            while (currentPoint.IsInRange(map.ActiveMapRange, false) && counter < maxLength)
            {
                Dictionary<Point, double> weigthsAround = GetPointsAround(map, currentPoint);
                if (!weigthsAround.Any())
                    break;
                var bestPoint = weigthsAround.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;

                if (map.Table[(int)currentPoint.X, (int)currentPoint.Y] >= map.Table[(int)bestPoint.X, (int)bestPoint.Y])
                    break;

                PointsMap.Add(bestPoint);

                UsedPoints.Add(bestPoint.Hash);
                currentPoint = bestPoint.Clone();
                counter++;
            }

            foreach (var mapP in PointsMap)
            {
                PointsWorld.Add(new Point(mapP.X * map.MapCellWidth, mapP.Y * map.MapCellWidth));
                RouteSum += map.Table[(int)mapP.X, (int)mapP.Y];
            }
        }


        private Dictionary<Point, double> GetPointsAround(BonusMap map, Point currentPoint)
        {
            var weigthsAround = new Dictionary<Point, double>();
            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(0, 1));
            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(1, 1));
            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(1, 0));

            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(0, -1));
            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(-1, -1));
            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(-1, 0));

            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(1, -1));
            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(-1, 1));

            return weigthsAround;
        }

        internal void CalculateWorldDestination(double radius)
        {
            var mapPointId = 0;
            var radiusSq = radius * radius;
            for (var i = PointsMap.Count - 1; i >= 0; i--)
            {
                if (Math.Abs(PointsMap[i].XYSum - StartPoint.XYSum) < radius)
                {
                    var distSq = GetStartEndDistSq(i);
                    if (distSq < radiusSq)
                    {
                        mapPointId = i;
                        break;
                    }
                }
            }
            if (mapPointId == 0)
            {
                NextWorldPoint = new Point(StartPoint.X * Map.MapCellWidth, StartPoint.Y * Map.MapCellWidth);
                return;
            }

            var distDebug = (PointsMap[mapPointId] - StartPoint).Length;

            var resultPointId = 0;
            for (var i = mapPointId; i <= PointsMap.Count - 1; i++)
            {
                var distSq = GetStartEndDistSq(i);

                if (distSq > radiusSq)
                {
                    resultPointId = i;
                    break;
                }

            }
            var mapPoint = PointsMap[resultPointId];

            NextMapPoint = mapPoint;
            NextWorldPoint = new Point(mapPoint.X * Map.MapCellWidth, mapPoint.Y * Map.MapCellWidth);
        }

        private double GetStartEndDistSq(int i)
        {
            var distVec = PointsMap[i] - StartPoint;
            var distSq = distVec.X * distVec.X + distVec.Y * distVec.Y;
            return distSq;
        }

        internal void ExtendTillRadius(double maxWorldStepPerTick)
        {
            if (PointsMap.Count < 2)
                return;
            var stepRange = new Range2(Math.Max(0, StartPoint.X - maxWorldStepPerTick),
                Math.Min(StartPoint.X + maxWorldStepPerTick, Map.XMapSize), 
                Math.Max(0, StartPoint.Y - maxWorldStepPerTick), 
                Math.Min(StartPoint.Y + maxWorldStepPerTick, Map.YMapSize));

            if (!PointsMap.Last().IsInRange(stepRange, false))
                return;

            int range = Math.Max(1, PointsMap.Count / 3);
            var dx = (PointsMap.Last().X - PointsMap[PointsMap.Count - 1 - range].X) / range;
            var dy = (PointsMap.Last().Y - PointsMap[PointsMap.Count - 1 - range].Y) / range;
            while (PointsMap.Last().IsInRange(stepRange, false))
                PointsMap.Add(new Point(PointsMap.Last().X + dx, PointsMap.Last().Y + dy));
        }

        private Dictionary<Point, double> AddWeigthToDict(Point currentPosition, Dictionary<Point, double> weigthsAround, Point shift)
        {
            var somePoint = currentPosition + shift;
            if (somePoint.IsInRange(Map.ActiveMapRange, false) && !UsedPoints.Contains(somePoint.Hash))
            {
                weigthsAround.Add(somePoint, Map.Table[(int)somePoint.X, (int)somePoint.Y]);
                //usedPoints.Add(somePoint.Hash);
            }
            return weigthsAround;
        }


    }
}
