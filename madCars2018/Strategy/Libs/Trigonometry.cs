﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public class Trigonometry
    {
        private Dictionary<int, double> SinTable = new Dictionary<int, double>();
        private const double Pi = Math.PI;

        public Trigonometry()
        {
            for (int i = 0; i <= 360; i++)
            {
                SinTable.Add(i, Math.Sin(i * Pi / 180));
            }

            var tt = $"Math - {MathSin(3600000)}, TrigInt - {TrigInt(3600000)}, TrigDouble - {TrigDouble(3600000)}";
        }

        private long TrigDouble(int ticks)
        {
            var timer = new Stopwatch();
            var random = new Random();
            timer.Start();
            for (var i = 0; i < ticks; i++)
            {
                var result = Sin(random.NextDouble() * 360);
            }
            timer.Stop();
            return timer.ElapsedMilliseconds;
        }

        private long TrigInt(int ticks)
        {
            var timer = new Stopwatch();
            var random = new Random();
            timer.Start();
            for (var i = 0; i < ticks; i++)
            {
                var result = Sin(random.Next(360));
            }
            timer.Stop();
            return timer.ElapsedMilliseconds;
        }

        private static long MathSin(int ticks)
        {
            var timer = new Stopwatch();
            var random = new Random();
            timer.Start();
            for (var i = 0; i < ticks; i++)
            {
                var result = Math.Sin(random.NextDouble() * 6.28318530718);
            }
            timer.Stop();
            return timer.ElapsedMilliseconds;
        }

        public double Sin (int angleDeg) =>  SinTable[angleDeg];
        //{
        //    double result;
        //
        //    if (SinTable.TryGetValue(angleDeg, out result))
        //        return result;
        //    //if (SinTable.ContainsKey(angleDeg))
        //    //    return SinTable[angleDeg];
        //
        //    result = Math.Sin(angleDeg * Pi / 180);
        //    SinTable.Add(angleDeg, result);
        //    return result;
        //}

        public double Sin (double angleDeg)
        {
            var minAngleDeg = (int)angleDeg;
            var maxAngleDeg = minAngleDeg + 1;

            var sinMin = Sin(minAngleDeg);
            var sinMax = Sin(maxAngleDeg);

            var result = sinMin + (sinMax - sinMin) * (angleDeg - minAngleDeg);

            return result;
        }

        public double Sin (Angle angle) => Sin(angle.ValueInDegrees);
    }
}
