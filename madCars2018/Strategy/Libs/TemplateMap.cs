﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public class TemplateMap : BonusMap
    {
        public TemplateMap(Range2 activeWorldRange, MapType mapType, int mapPointsAmount) : base(activeWorldRange, MapType.Final, mapPointsAmount) { }

    }
}
