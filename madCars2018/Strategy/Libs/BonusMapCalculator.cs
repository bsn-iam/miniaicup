﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;


namespace Strategy
{
    public class BonusMapCalculator : BonusMapCalculatorBase
    {
        public List<BonusMap> BonusMapList { get; private set; } = new List<BonusMap>();
        public BonusMap ResultingMap { get; private set; }

        public BonusMapCalculator()
        {
        }

        public void AddMap(BonusMap map)
        {
            BonusMapList.Add(map);
        }

        public void SetActiveRange(Point centerPosition, double maxWorldStepPerTick, double mapVisabilityKoeff, double mapResolutionForWorldStep)
        {
            var templateMap = GenerateDynamicTemplateMap(centerPosition, maxWorldStepPerTick, mapVisabilityKoeff, mapResolutionForWorldStep);
            foreach (var map in BonusMapList)
                if (map.IsDynamic) {
                    map.UpdateRangeFromTemplate(templateMap);
                }
        }

        public void RegenerateMapList(WorldState worldState)
        {
            var timer = new Stopwatch();
            foreach (var map in BonusMapList)
            {
                timer.Restart();
                if (map.IsValid(worldState) && map.IsOutdated(worldState))
                    map.RegenerateMap(worldState);
                map.SetTime(timer.ElapsedMilliseconds);
            }
            ResultingMap = new ResultingMap(BonusMapList);
        }

        public double CalculateReward(Point worldPoint, WorldState worldState)
        {
            var timer = new Stopwatch();
            double reward = 0;
            foreach (var map in BonusMapList)
            {
                timer.Restart();
                if (map.IsValid(worldState) && map.IsOutdated(worldState))
                {
                    var clearReward = map.CalculateRewardInWorldPoint(worldPoint, worldState);
                    if (clearReward > 1 || clearReward < 0)
                        throw new Exception($"Wrong map reward calcluation. Value should be in the range [0;1] for map [{map.Name}].");
                    reward += clearReward * map.Weight;
                }
                    
                map.SetTime(timer.ElapsedMilliseconds);
            }
            return reward;
        }

        public static BonusMap GenerateDynamicTemplateMap(Point objectPosition, double maxWorldStepPerTick, double mapVisabilityKoeff, double mapResulutionForWorldStep)
        {
            var activeMapRadius = mapResulutionForWorldStep * mapVisabilityKoeff;

            var xWorldRange = new Range(0, Strategy.Constants.GameWidth);
            var yWorldRange = new Range(0, Strategy.Constants.GameHeight);

            var activeWorldRadius = 1.1 * maxWorldStepPerTick * mapVisabilityKoeff;
            var mapPointsAmount = (int)Math.Min(xWorldRange.MaxLimit, xWorldRange.MaxLimit / (activeWorldRadius / activeMapRadius));

            var activeWorldRange = new Range2(xWorldRange.ValueClump(objectPosition.X - activeWorldRadius),
                                            xWorldRange.ValueClump(objectPosition.X + activeWorldRadius),
                                            yWorldRange.ValueClump(objectPosition.Y - activeWorldRadius),
                                            yWorldRange.ValueClump(objectPosition.Y + activeWorldRadius));

            var mapTemplate = new TemplateMap(activeWorldRange, MapType.Final, mapPointsAmount);

            return mapTemplate;
        }

    }
}
