﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public class Branch
    {
        public WorldState InitialState { get; private set; }
        public WorldState FinalState { get; set; }
        public Order Order { get; private set; }
        public List<Point> Route { get; set; } = new List<Point>();
        public List<int> ChildIDs { get; private set; } = new List<int>();
        public bool IsLineEnd => !ChildIDs.Any();

        public void AddChild(int childBranchId)
        {
            ChildIDs.Add(childBranchId);
        }

        internal void SetParent(int parentId)
        {
            ParentId = parentId;
        }

        public double Reward { get; set; } = 0;
        public WorldState ForkState => FinalState;
        public int ParentId { get; private set; } = 0;
        public bool IsRoot { get; } = false;
        public int Id { get; internal set; } = 0;

        public Branch (WorldState initialState, Order command, int id, bool isRoot = false)
        {
            this.InitialState = initialState;
            this.Order = command;
            this.Id = id;
            this.IsRoot = isRoot;
        }
    }


    public class Order
    {
        public Vector Vector { get; }
        public Order(Vector vector)
        {
            Vector = vector; 
        }

    }
}
