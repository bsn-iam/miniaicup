﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public class LinearPredictorState
    {
        public List<IPredictable> Units { get; }
        public int Id { get; }
        public bool IsRealValue { get; set; }
        public long CalculationTime { get; private set; }

        public LinearPredictorState(List<IPredictable> units, bool isRealValue)
        {
            Units = units;
            IsRealValue = isRealValue;
            Id = Guid.NewGuid().GetHashCode();
        }

        public LinearPredictorState()
        {
            Id = Guid.NewGuid().GetHashCode();
        }

        internal LinearPredictorState SetTime(long elapsedMilliseconds)
        {
            CalculationTime = elapsedMilliseconds;
            return this;
        }
    }
}
