﻿using System;
using System.Collections.Generic;

namespace Strategy
{
    public class TileGenerator
    {
        private double TileWidth;

        public TileGenerator()
        {
            //this.TileWidth = tileWidth;
        }

        public IEnumerable<Tile> GetTileList(BonusMap map)
        {
            TileWidth = map.MapCellWidth;
            var hasRealValues = map.RealTable != null ? true : false;
            map.Trim();
            var tileList = new List<Tile>();
            for (int i = map.ActiveMapRange.XMin; i < map.ActiveMapRange.XMax; i++)
                for (int j = map.ActiveMapRange.YMin; j < map.ActiveMapRange.YMax; j++)
                {
                    var tileCenter = new Point(i * map.MapCellWidth + TileWidth / 2,
                    j * map.MapCellWidth + TileWidth / 2);
                    if (hasRealValues)
                        tileList.Add(new Tile(tileCenter, TileWidth, map.Table[i, j], map.RealTable[i, j]));
                    else
                        tileList.Add(new Tile(tileCenter, TileWidth, map.Table[i, j]));

                    if (map.Table[i, j] > 1 || map.Table[i, j] < 0)
                        throw new Exception("Wrong tile trim.");
            }
            return tileList;
        }
    }

    public class Tile
    {
        public Tile(Point centerPosition, double size, double value, double realValue = 0)
        {
            CenterPosition = centerPosition;
            Value = value;
            Size = size;
            RealValue = realValue;
        }

        public Point CenterPosition { get; set; }
        public double Value { get; set; }
        public double RealValue { get; set; }
        public double Size { get; set; }
    }
}
