﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;


namespace Strategy
{
    public class BonusMapCalculator : BonusMapCalculatorBase
    {
        private static BonusMap MapTemplate;

        public Point GenerateTargetPoint(Ball ball, List<Point> foodList, List<Ball> enemyBalls)
        {
            var timer = new Stopwatch();
            timer.Start();

            //var angleStep = 5; //degrees
            var maxWorldStepPerTick = Strategy.Constants.SpeedFactor / Math.Sqrt(ball.Mass); //max speed

            var mapVisabilityKoeff = 1.0; // 1 - Points amout equals the minimum required for world step.
            var mapResulutionForWorldStep = 7.0;
            MapTemplate = GenerateTemplateMap(ball, maxWorldStepPerTick, mapVisabilityKoeff, mapResulutionForWorldStep);

            var mapGenerationTimer = new Stopwatch();
            mapGenerationTimer.Start();
            ball.BonusMapList = GenerateMapList(ball, foodList, enemyBalls);
            mapGenerationTimer.Stop();

            //var raysAndDestinationTimer = new Stopwatch();
            //raysAndDestinationTimer.Start();
            //    ball.PossibleRays = GeneratePossibleRays(ball.BonusMapList.Last(), new Point(ball.X, ball.Y), maxWorldStepPerTick, angleStep);
            //    var chosenDestination = GetBestDestination(ball.PossibleRays);
            //raysAndDestinationTimer.Stop();

            ball.MapRoute = new Route(ball.BonusMapList.Last(), new Point(ball.X, ball.Y), 500);
            ball.MapRoute.ExtendTillRadius(maxWorldStepPerTick);

            var chosenDestination = ball.MapRoute.CalculateWorldDestination(maxWorldStepPerTick);
            timer.Stop();

            ball.MapCalculationTime = timer.ElapsedMilliseconds;
            ball.TargetPoint = chosenDestination;
            return chosenDestination;
        }

        private static BonusMap GenerateTemplateMap(Ball ball, double maxWorldStepPerTick, double mapVisabilityKoeff, double mapResulutionForWorldStep)
        {
            var activeMapRadius = mapResulutionForWorldStep * mapVisabilityKoeff;

            var xWorldRange = new Range(0, Strategy.Constants.GameWidth);
            var yWorldRange = new Range(0, Strategy.Constants.GameHeight);



            var activeWorldRadius = 1.1 * maxWorldStepPerTick * mapVisabilityKoeff;
            var mapPointsAmount = (int)Math.Min(xWorldRange.MaxLimit, yWorldRange.MaxLimit / (activeWorldRadius / activeMapRadius));

            var activeRange = new Range2(xWorldRange.ValueClump(ball.X - activeWorldRadius),
                                            xWorldRange.ValueClump(ball.X + activeWorldRadius),
                                            yWorldRange.ValueClump(ball.Y - activeWorldRadius),
                                            yWorldRange.ValueClump(ball.Y + activeWorldRadius));

            var mapTemplate = new BonusMap(
                (int)xWorldRange.MaxLimit,
                (int)yWorldRange.MaxLimit, mapPointsAmount,
                activeRange,
                MapType.Final);

            return mapTemplate;
        }

        private List<BonusMap> GenerateMapList(Ball ball, List<Point> foodList, List<Ball> enemyBalls)
        {
            var mapTimer = new Stopwatch();
            var bonusMapList = new List<BonusMap>();

            //Rotation map

            mapTimer.Restart();
            var rotationMap = GenerateRotationMap(MapTemplate, ball);
            bonusMapList.Add(rotationMap.SetWeight(0.3).SetTime(mapTimer.ElapsedMilliseconds).SetName("Rotation map"));


            //Food map
            mapTimer.Restart();
            if (foodList.Any())
            {
                var foodMap = new BonusMap(MapTemplate, MapType.Flat);
                //var visability = 2.5 * radius * Math.Sqrt(fragments.count());
                
                foreach (var food in foodList)
                {
                    var zeroDistance = Math.Max(new Point(food.X - ball.X, food.Y - ball.Y).Length, 4* ball.Radius);
                    foodMap.AddUnitCalculation(new Point(food.X, food.Y), ball.Radius * 0.7, Strategy.Constants.FoodMass, zeroDistance);
                }
                bonusMapList.Add(foodMap.SetWeight(2).SetTime(mapTimer.ElapsedMilliseconds).SetName("Food map"));
            }

            //Enemy map
            mapTimer.Restart();
            if (enemyBalls.Any(eb => ball.Mass > 1.3 * eb.Mass || 1.1 * ball.Mass < eb.Mass)) //Duplication in TargetGenerator!
            {
                var predictedWorldState = Strategy.Predictor.GetStateOnTick(Strategy.TickIndex + 1, Strategy.TickIndex);
                var predictedEnemyUnits = predictedWorldState.Units;

                foreach (var enemyBall in predictedEnemyUnits)
                {
                    var enemyMap = new BonusMap(MapTemplate, MapType.Additive);
                    enemyMap.AddUnitCalculation(new Point(enemyBall.X, enemyBall.Y), enemyBall.Radius * 0.5, 1, 8 * ball.Radius);
                    bonusMapList.Add(enemyMap.SetWeight(15 * (ball.Mass - enemyBall.Mass) / ball.Mass).SetTime(mapTimer.ElapsedMilliseconds).SetName($"{enemyBall.Id} enemy map"));
                }
            }

            //Border map
            mapTimer.Restart();
            var ballPosition = new Point(ball.X, ball.Y);
            var safeZone = 2 * ball.Radius;

            var noBorderRange = new Range2(safeZone, Strategy.Constants.GameWidth - safeZone, safeZone, Strategy.Constants.GameHeight - safeZone);
            if (!ballPosition.IsInRange(noBorderRange))
            {
                var borderWeight = -1.5;
                foreach (var bonusMap in bonusMapList)
                {
                    if (Math.Abs(bonusMap.Weight) > Math.Abs(borderWeight))
                        borderWeight = -Math.Abs(bonusMap.Weight);
                }

                var borderMap = GenerateBallBorderMap(1 * ball.Radius, 2 * ball.Radius);

                if (borderWeight >= 0)
                    throw new Exception("Wrong border map weigth!");
                bonusMapList.Add(borderMap.SetWeight(borderWeight).SetTime(mapTimer.ElapsedMilliseconds).SetName("Border map"));
            }

            //SetRealTables(bonusMapList); //debug only

            //Resulting map
            mapTimer.Restart();
            var resultingMap = CalculateResultingMap(bonusMapList);
            bonusMapList.Add(resultingMap.SetTime(mapTimer.ElapsedMilliseconds).SetName("Resulting map"));

            return bonusMapList;
        }



        private BonusMap GenerateBallBorderMap(double borderWidth, double circleRadius)
        {
            var borderMap = new BonusMap(MapTemplate, MapType.Flat);
            return GenerateBorderMap(borderMap, borderWidth, circleRadius);
        }


        private BonusMap GenerateRotationMap(BonusMap templateMap, Ball ball)
        {
            var resultMap = new BonusMap(templateMap, MapType.Additive);
            var maxStepPerTick = Strategy.Constants.SpeedFactor / Math.Sqrt(ball.Mass); //max speed
            var bestTargetWorldPoint = new Point(ball.X + ball.SpeedX * maxStepPerTick, ball.Y + ball.SpeedY * maxStepPerTick);

            var targetMapPoint = new Point(bestTargetWorldPoint.X / MapTemplate.MapCellWidth, bestTargetWorldPoint.Y / MapTemplate.MapCellWidth);

            for (int x = resultMap.ActiveMapRange.XMin; x < resultMap.ActiveMapRange.XMax; x++)
                for (int y = resultMap.ActiveMapRange.YMin; y < resultMap.ActiveMapRange.YMax; y++)
            {
                var dx = Math.Abs(x - targetMapPoint.X);
                var dy = Math.Abs(y - targetMapPoint.Y);
                var dSum = dx + dy;
                if (dSum < maxStepPerTick)
                    templateMap.Table[x, y] = maxStepPerTick - dSum;
            }

            return templateMap;
        }

    }
}
