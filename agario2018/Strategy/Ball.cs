﻿
using System;
using System.Collections.Generic;

namespace Strategy
{
    public class Ball
    {

        // уникальный идентификатор (string)
        // для игроков через точку записывается номер фрагмента (если есть)
        //"Id": "1.1",

        // координаты в пространстве (float)
        //"X": 100.0, "Y": 100.0,

        // радиус и масса (float)
        //"R": 8.0, "M": 40.0,

        // скорость в проекциях Ox и Oy (float)
        //"SX": 0.365, "SY": 14.0,

        // таймер слияния (int) (если есть)
        // "TTF": 250,

        public string Id { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Radius { get; set; }
        public double Mass { get; set; }
        public double SpeedX { get; set; }
        public double SpeedY { get; set; }
        public int MergeTimer { get; set; }

        public List<BonusMap> BonusMapList { get; set; } = new List<BonusMap>();
        public Dictionary<Point, double> PossibleRays { get; internal set; } = new Dictionary<Point, double>();
        public long MapCalculationTime { get; internal set; } = 0;
        public Point TargetPoint { get; internal set; } = new Point();
        public Point InertialOrder { get; internal set; } = new Point();
        public bool IsActive { get; internal set; } = false;
        public double TargetPointOrientationAngle { get; internal set; } = 0;
        public double TargetResultAngle { get; internal set; } = 0;
        public Point Prediction { get; internal set; } = new Point();
        public Route MapRoute { get; internal set; }

        public Point BestTargetWorldPoint()
        {
            //var maxStepPerTick = Strategy.Constants.SpeedFactor / Math.Sqrt(Mass); //max speed
            var maxStepPerTick = 4 * Radius;
            var bestTargetWorldPoint = new Point(X + SpeedX * maxStepPerTick, Y + SpeedY * maxStepPerTick);
            return bestTargetWorldPoint;
        }

        public Ball Clone()
        {
            return new Ball()
            {
                X = this.X,
                Y = this.Y,
                Id = this.Id,
                Radius = this.Radius,
                Mass = this.Mass,
                SpeedX = this.SpeedX,
                SpeedY = this.SpeedY,
                MergeTimer = this.MergeTimer,
        };
        }
    }
}
