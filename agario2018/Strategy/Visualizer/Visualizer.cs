﻿//using MyStrategy;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Strategy.Visualizer
{
    public static partial class Visualizer
    {
        public static Universe Universe;
        public static List<long> StepTimeList = new List<long>();

        public static void _draw(Universe universe)
        {
            try
            {
                Universe = universe;
                var panel = _form.panel;

                double worldHeight = Strategy.Constants.GameHeight;
                double worldWidth = Strategy.Constants.GameWidth;

                var bonusMapId = 0;
                var ballNo = 0;
                int.TryParse(_form.mapIdTextBox.Text.Trim(), out bonusMapId);
                int.TryParse(_form.lookAtTextBox.Text.Trim(), out ballNo);
                var myBalls = Universe.Balls;
                if (ballNo > myBalls.Count)
                    ballNo = 0;

                Ball lookAtBall = null;
                if (myBalls.Any())
                    //lookAtBall = myBalls[ballNo - 1];
                    lookAtBall = myBalls.FirstOrDefault(b => b.IsActive);

                if (lookAtBall != null)
                    LookAt(new Point(lookAtBall.X, lookAtBall.Y), worldWidth, worldHeight);

                var drawArea = new Bitmap((int)worldWidth, (int)worldHeight);
                panel.Image = drawArea;
                _graphics = Graphics.FromImage(drawArea);

                //var calculationTime = Strategy.Strategy.MainTimer.ElapsedMilliseconds;
                //_form.tickLabel.Text = $"{calculationTime / 1000:f1} kms";

                //if (lookAtBall != null)
                //    _form.MainStepTime.Text = $"{lookAtBall.MapCalculationTime:f2} ms";
                StepTimeList.Add(Universe.MainTimer);
                if (StepTimeList.Count > 1000) StepTimeList.RemoveAt(0);

                _form.MainStepTime.Text = $"{StepTimeList.Average():f2} ms";
                _form.VisualStepTime.Text = $"{visualTimer.ElapsedMilliseconds / 1000:f1} ms";
                _form.TickIndex.Text = $"{Universe.TickIndex}";

                #region BonusMap

                if (lookAtBall != null && lookAtBall.BonusMapList.Any())
                {
                    if (bonusMapId >= lookAtBall.BonusMapList.Count)
                        bonusMapId = lookAtBall.BonusMapList.Count - 1;

                    var tileGenerator = new TileGenerator();
                    var requiredMap = lookAtBall.BonusMapList[bonusMapId];
                    var tileList = tileGenerator.GetTileList(requiredMap);

                    foreach (var tile in tileList)
                        if (Math.Abs(tile.Value) > Double.Epsilon)
                        {
                            var color01 = new Visualizer.Color01(1, 1 - tile.Value, 1 - tile.Value);
                            Visualizer.FillRect(color01.ToColor(), tile.CenterPosition.X, tile.CenterPosition.Y,
                                tile.Size * 1, tile.Size * 1);

                            if (tile.Value > 0.9999)
                                Visualizer.FillRect(Color.YellowGreen, tile.CenterPosition.X, tile.CenterPosition.Y,
                                    tile.Size * 1, tile.Size * 1);

                            if (Math.Abs(tile.RealValue) > Double.Epsilon)
                                Visualizer.DrawText($"{tile.RealValue:f2}", 3, Brushes.Black, tile.CenterPosition.X,
                                    tile.CenterPosition.Y);
                        }
                }

                var rays = lookAtBall.PossibleRays;
                if (lookAtBall != null && lookAtBall.PossibleRays.Any())
                {

                    var maxRay = rays.OrderByDescending(r => r.Value).FirstOrDefault();
                    foreach (var ray in rays)
                    {
                        var color01 = new Color01(0, 1 - ray.Value, 1 - ray.Value);
                        var color02 = new Color01(0, ray.Value * ray.Value * ray.Value,
                            (1 - ray.Value) * (1 - ray.Value) * (1 - ray.Value));
                        if (ray.Key == maxRay.Key)
                            FillCircle(Color.CornflowerBlue, ray.Key.X, ray.Key.Y, 1);
                        //DrawCircle(color02.ToColor(), ray.Key.X, ray.Key.Y, 1);
                        FillCircle(color02.ToColor(), ray.Key.X, ray.Key.Y, 0.8);
                        //DrawCircle(Color.Black, ray.Key.X, ray.Key.Y, 4);
                    }

                }



                #endregion

                #region Vision range

                foreach (var myBall in universe.Balls)
                {
                    Visualizer.DrawCircle(Color.DeepSkyBlue, myBall.X, myBall.Y, 4 * myBall.Radius);
                    DrawCircle(Color.LightGreen, myBall.X, myBall.Y, myBall.Radius, 2);
                }

                #endregion

                #region Food

                var foodList = Universe.VisibleFoodPoints;
                if (foodList.Any())
                    foreach (var food in foodList)
                        FillCircle(Color.Green, food.X, food.Y, 4 * Strategy.Constants.FoodMass);
                #endregion

                #region Enemies
                var enemyBalls = Universe.EnemyBalls;
                if (enemyBalls.Any())
                    foreach (var enemyBall in enemyBalls)
                        DrawCircle(Color.Black, enemyBall.X, enemyBall.Y, enemyBall.Radius, 2);

                var predictedState = universe.WorldStateList.LastOrDefault();
                var predictedEnemyBalls = predictedState.Value.Units;
                if (predictedEnemyBalls.Any())
                    foreach (var enemyBall in predictedEnemyBalls)
                        DrawCircle(Color.Gray, enemyBall.X, enemyBall.Y, enemyBall.Radius, 2);


                #endregion

                #region Orders

                DrawLine(Color.LightGreen, lookAtBall.X, lookAtBall.Y, lookAtBall.TargetPoint.X, lookAtBall.TargetPoint.Y, 2);
                DrawLine(Color.DeepSkyBlue, lookAtBall.X, lookAtBall.Y, lookAtBall.X + 10 * lookAtBall.SpeedX, lookAtBall.Y + 10 * lookAtBall.SpeedY, 2);
                DrawLine(Color.DarkBlue, lookAtBall.X, lookAtBall.Y, lookAtBall.InertialOrder.X, lookAtBall.InertialOrder.Y, 2);
                DrawLine(Color.Yellow, lookAtBall.X, lookAtBall.Y, lookAtBall.Prediction.X, lookAtBall.Prediction.Y, 2);

                DrawText($"TCurrent: {lookAtBall.TargetPointOrientationAngle:f0}", 2, Brushes.DeepSkyBlue, lookAtBall.X + 10 * lookAtBall.SpeedX, lookAtBall.Y + 10 * lookAtBall.SpeedY);
                DrawText($"TResult: {lookAtBall.TargetResultAngle:f0}", 2, Brushes.Yellow, lookAtBall.Prediction.X, lookAtBall.Prediction.Y);

                #endregion

                if (lookAtBall != null)
                {
                    foreach (var point in lookAtBall.MapRoute.Points)
                    {
                        FillCircle(Color.Azure, point.X, point.Y, 0.5);
                    }
                    var destPoint = lookAtBall.MapRoute.NextPoint;
                    FillCircle(Color.LightSeaGreen, destPoint.X, destPoint.Y, 0.5);
                }

                if (Strategy.BallBody != null)
                {
                    var bPos = Strategy.BallBody.Position;
                    FillRect(Color.Cyan, bPos.X, bPos.Y, 10, 10);
                }

                #region MapRanges

                // map ranges
                Visualizer.DrawLine(Color.Black, 1, 1, 1, worldHeight - 1);
                Visualizer.DrawLine(Color.Black, 1, worldHeight - 1, worldWidth - 1, worldHeight - 1);
                Visualizer.DrawLine(Color.Black, worldWidth - 1, 1, worldWidth - 1, worldHeight - 1);
                Visualizer.DrawLine(Color.Black, worldWidth - 1, 1, 1, 1);

                #endregion



                foreach (var seg in Visualizer.SegmentsDrawQueue)
                {
                    var points = seg[0] as List<System.Drawing.Point>;
                    var pen = seg[1] as Pen;
                    float width = seg.Length > 2 ? Convert.ToSingle(seg[2]) : 0F;
                    for (var i = 1; i < points.Count; i++)
                        Visualizer.DrawLine(pen.Color, points[i].X, points[i].Y, points[i - 1].X, points[i - 1].Y, width);
                }

            }
            catch {}
        }

        //public static void DrawEnemyUnit(Color color, Vehicle unit, string label)
        //{
        //    FillCircle(color, unit.X, unit.Y, 1);
        //    FillCircle(Color.Black, unit.X, unit.Y, 2);
        //    // DrawCircle(Color.CornflowerBlue, unit.X, unit.Y, unit.AerialAttackRange);
        //    // DrawCircle(Color.RosyBrown, unit.X, unit.Y, unit.GroundAttackRange);
        //    DrawText(label, 2, Brushes.Black, unit.X, unit.Y);
        //}
        //public static void DrawUnit(Color color, Vehicle unit, string label)
        //{
        //    FillCircle(color, unit.X, unit.Y, 2);
        //    // DrawCircle(Color.CornflowerBlue, unit.X, unit.Y, unit.AerialAttackRange);
        //    // DrawCircle(Color.RosyBrown, unit.X, unit.Y, unit.GroundAttackRange);
        //    DrawText(label, 2, Brushes.Black, unit.X, unit.Y);
        //}
        //
    }


}
