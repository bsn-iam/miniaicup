﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;


namespace Strategy
{
    public class Predictor
    {
        public SortedList<int, WorldState> WorldStateList { get; internal set; } = new SortedList<int, WorldState>();

        internal void RunTick(int currentTickIndex, IEnumerable<Ball> units)
        {
            //clear all predictions
            foreach (var state in new SortedList<int, WorldState>(WorldStateList))
                if (!state.Value.IsRealValue)
                    WorldStateList.Remove(state.Key);

            //clear all old reals state values
            foreach (var state in new SortedList<int, WorldState> (WorldStateList))
                if (state.Key < currentTickIndex - 5)
                    WorldStateList.Remove(state.Key);

            WorldStateList.Add(currentTickIndex, new WorldState(new List<Ball>(units), true));

        }

        public WorldState GetStateOnTick(int tick, int currentTickIndex)
        {
            foreach (var state in WorldStateList)
                if (state.Key == tick)
                    return WorldStateList[tick];

            WorldStateList.Add(tick, IntegrateTillTick(tick, currentTickIndex));
            return WorldStateList[tick];
        }

        private WorldState IntegrateTillTick(int tick, int currentTickIndex)
        {
            var timer = new Stopwatch();
            timer.Start();
            var predictedOppUnits = new List<Ball>();

            var allRealUnits = WorldStateList[currentTickIndex].Units; // If !Any()  -- RunTick() was not called.

            foreach (var unit in allRealUnits)
            {
                var unitSpeed = CalculateUnitSpeed(unit, currentTickIndex);

                var predictedX = unit.X + unitSpeed.SpeedX * (tick - currentTickIndex);
                var predictedY = unit.Y + unitSpeed.SpeedY * (tick - currentTickIndex);

                var predictedUnit = unit.Clone();
                predictedUnit.X = predictedX;
                predictedUnit.Y = predictedY;

                predictedOppUnits.Add(predictedUnit);
            }
            timer.Stop();
            return new WorldState(predictedOppUnits, false).SetTime(timer.ElapsedMilliseconds);
        }

        private Speed CalculateUnitSpeed(Ball unit, int currentTickIndex)
        {
            var unitSpeed = new Speed(0, 0);
            if (WorldStateList.LastOrDefault().Value == null)
                return unitSpeed;
            
            var stateOld = WorldStateList.LastOrDefault(s => s.Value.IsRealValue && s.Key < currentTickIndex - 2);

            if (stateOld.Value == null)
                return unitSpeed;

            var unitOld = stateOld.Value.Units.FirstOrDefault(u => u.Id.Equals(unit.Id));

            if (unitOld == null)
                return unitSpeed;

            var ticksBetweenStates = currentTickIndex - stateOld.Key;

            if (ticksBetweenStates > 0)
                unitSpeed = new Speed((unit.X - unitOld.X) / ticksBetweenStates,
                    (unit.Y - unitOld.Y) / ticksBetweenStates);
            else
                unitSpeed = new Speed(0, 0);

            return unitSpeed;
        }
    }

    public class WorldState
    {
        public WorldState(List<Ball> units, bool isRealValue)
        {
            Units = units;
            IsRealValue = isRealValue;

            //if (Units.Any())
            //    unitType = units.First().GetType();

        }

        //private readonly Type unitType;
        //public List<ISimpleUnit> SimpleUnits  { get; }
        public List<Ball> Units  { get; }

        //public List<dynamic> Units {
        //    get
        //    {
        //        var list = new List<object>();
        //        foreach (var simpleUnit in SimpleUnits)
        //            list.Add(Convert.ChangeType(simpleUnit, unitType));
        //
        //        return list;
        //    }
        //}

        public bool IsRealValue { get; set; }
        public long CalculationTime { get; private set; }

        internal WorldState SetTime(long elapsedMilliseconds)
        {
            CalculationTime = elapsedMilliseconds;
            return this;
        }
    }

    public class Speed
    {
        public Speed(double speedX, double speedY)
        {
            this.SpeedX = speedX;
            this.SpeedY = speedY;
        }

        public double SpeedX { get; set; }
        public double SpeedY { get; set; }

    }
}
