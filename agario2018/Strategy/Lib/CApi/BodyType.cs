﻿
namespace Strategy.Lib.CApi {
    public enum BodyType {
        DYNAMIC,
        KINEMATIC,
        STATIC,
    }
}
