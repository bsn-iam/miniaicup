﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json.Linq;
using Strategy.Lib.CSharp;
using Strategy.Lib.CApi;

namespace Strategy
{
    public class Strategy
    {
        public static Constants Constants;
        public static TargetGenerator TGen = new TargetGenerator();
        public static Stopwatch MainTimer = new Stopwatch();
        public static List<Point> VisibleFoodPoints = new List<Point>();
        public static List<Ball> Balls = new List<Ball>();
        public static List<Ball> EnemyBalls = new List<Ball>();
        public static Predictor Predictor = new Predictor();
        public static int TickIndex = 0;

        public static Universe Universe;
        public static Body BallBody;


        public static void Main(String[] args)
        {
            MainTimer.Start();
            GetConstants();
            BallBody = ChipmunkInit();

            while (true)
            {
                MainTimer.Restart();
                TickIndex += 1;
                string data;
#if DEBUG
                data = FixedConsole.ReadLine();
                Visualizer.Visualizer.CreateForm();

                ChipmunkIncrement();
                //TryChipmunkSpace();
#else
                data = Console.ReadLine();
#endif

                var parsed = JObject.Parse(data);
                JObject command=new JObject();
#if DEBUG
                command = OnTick(parsed);

                //if (mainTimer.ElapsedMilliseconds > 150000)
                //    System.Diagnostics.Debugger.Launch();
                if (TickIndex % 2 == 0)
                {
                    MainTimer.Stop();
                    Universe = new Universe(Balls, EnemyBalls, Predictor.WorldStateList, VisibleFoodPoints, MainTimer.ElapsedMilliseconds, TickIndex);
                    Visualizer.Visualizer.Draw();
                    MainTimer.Start();
                }

#else
                try
                {
                    command = OnTick(parsed);

                }
                catch (Exception ex)
                {
                    command["X"] = 0;
                    command["Y"] = 0;
                    command["Debug"] = ex.Message + ex.StackTrace;
                }
#endif
                MainTimer.Stop();
                Console.WriteLine(command.ToString(Newtonsoft.Json.Formatting.None));
                
            }
        }

        private static Body ChipmunkInit()
        {
            var space = new Space();

            space.Gravity = new Vect(0, 100);
            var shape = new SegmentShape(space.StaticBody, new Vect(-20, 5), new Vect(20, -5), 0);
            shape.Friction = 1;

            space.AddShape(shape);

            double radius = 5;
            double mass = 1;
            double moment = Utils.MomentForCircle(mass, 0, radius, Vect.Zero);

            var ballBody = new Body(mass, moment);
            space.AddBody(ballBody);
            ballBody.Position = new Vect(150, 50);

            var ballShape = new CircleShape(ballBody, radius, Vect.Zero);
            space.AddShape(ballShape);
            ballShape.Friction = 0.7;

            return ballBody;
        }

        public static void ChipmunkIncrement()
        {
            var pos = BallBody.Position;
            var vel = BallBody.Velocity;

            if (pos.Y > 300)
                BallBody.Space.Gravity = new Vect(0, -100);

            if (pos.Y < 250)
                BallBody.Space.Gravity = new Vect(0, 100);

            double timeStep = 1 / 60d;
            BallBody.Space.Step(timeStep);
            if (pos.Y > 600)
                BallBody.Position = new Vect(150, 600);


            if (pos.Y < 30)
                BallBody.Position = new Vect(150, 30);
        }

        private static void TryChipmunkSpace()
        {
            var space = new Space();
            
            space.Gravity = new Vect(0, 100);
            var shape = new SegmentShape(space.StaticBody, new Vect(-20, 5), new Vect(20, -5), 0);
            shape.Friction = 1;

            space.AddShape(shape);

            double radius = 5;
            double mass = 1;
            double moment = Utils.MomentForCircle(mass, 0, radius, Vect.Zero);

            var ballBody = new Body(mass, moment);
            space.AddBody(ballBody);
            ballBody.Position = new Vect(0, 15);

            var ballShape = new CircleShape(ballBody, radius, Vect.Zero);
            space.AddShape(ballShape);
            ballShape.Friction = 0.7;

            double timeStep = 1 / 60d;
            var results = new List<string>();
            for (double time = 0; time < 2; time += timeStep)
            {
                var pos = ballBody.Position;
                var vel = ballBody.Velocity;
                results.Add($"Time is {time:F2}. ballBody is at ({pos.X:F2}, {pos.Y:F2}). It's velocity is ({vel.X:F2}, {vel.Y:F2})");
               
                if (pos.Y > 350)
                    space.Gravity = new Vect(0, -100);

                if (pos.Y < 300)
                   space.Gravity = new Vect(0, 100);

                ballBody.Space.Step(timeStep);
            }
        }

        private static void GetConstants()
        {
            string configData;

#if DEBUG
            configData = FixedConsole.ReadLine();
#else
            configData = Console.ReadLine();
#endif

            var config = JObject.Parse(configData);
            Constants = new Constants()
            {
                GameWidth = (double) config["GAME_WIDTH"],
                GameHeight = (double) config["GAME_HEIGHT"],
                FoodMass = (double) config["FOOD_MASS"],
                MaxFragsCount = (int) config["MAX_FRAGS_CNT"],
                TicksTillFusion = (int) config["TICKS_TIL_FUSION"],
                VirusRadius = (double) config["VIRUS_RADIUS"],
                VirusSplitMass = (double) config["VIRUS_SPLIT_MASS"],
                Viscosity = (double) config["VISCOSITY"],
                InertionFactor = (double) config["INERTION_FACTOR"],
                SpeedFactor = (double) config["SPEED_FACTOR"],
            };
        }

        public static JObject OnTick(JObject parsed)
        {
            var mineObjects = (JArray)parsed.GetValue("Mine");
            var objects = (JArray)parsed.GetValue("Objects");
            VisibleFoodPoints = FindVisibleFood(objects);
            EnemyBalls = FindEnemyBalls(objects);
            Predictor.RunTick(TickIndex, EnemyBalls);

            var result = new JObject();
            if (mineObjects.Count > 0)
            {
                GetMyBalls(mineObjects);

                var currentBall = GetCurrentBall();
                result = TGen.GenerateTarget(currentBall, VisibleFoodPoints, EnemyBalls, objects, MainTimer.ElapsedMilliseconds);

            }
            else
            {
                Balls = new List<Ball>(); 
                result["X"] = 0;
                result["Y"] = 0;
                result["Debug"] = "I was eaten";
            }

            return result;
        }

        static IEnumerable<I> GetInterfaceEnumerator<T, I>(IList<T> lst) where T : I
        {
            foreach (I item in lst) yield return item;
        }

        private static Ball GetCurrentBall()
        {
            var currentBall = EnemyBalls.Any() ? FindClosestToEnemyBall() : Balls.OrderBy(b => b.Mass).First();

            foreach (var ball in Balls)
                ball.IsActive = false;
            currentBall.IsActive = true;
            return currentBall;
        }

        private static Ball FindClosestToEnemyBall()
        {
            var ballToEnemyDistance = new Dictionary<Ball, double>();
            foreach (var ball in Balls)
                foreach (var enemyBall in EnemyBalls)
                {
                    var squaredDistance = (enemyBall.X - ball.X) * (enemyBall.X - ball.X) + (enemyBall.Y - ball.Y) * (enemyBall.Y - ball.Y);
                    if (!ballToEnemyDistance.ContainsKey(ball))
                        ballToEnemyDistance.Add(ball, squaredDistance);
                    else
                    if (ballToEnemyDistance[ball] > squaredDistance)
                            ballToEnemyDistance[ball] = squaredDistance;
                }

            var minDistance = Double.MaxValue;
            var closestBall = ballToEnemyDistance.First().Key;
            foreach (var ballDist in ballToEnemyDistance)
            {
                if (ballDist.Value < minDistance)
                {
                    minDistance = ballDist.Value;
                    closestBall = ballDist.Key;
                }
            }
            return closestBall;
        }

        private static void GetMyBalls(JArray mineObjects)
        {
            var newBalls = new List<Ball>();
            foreach (var obj in mineObjects)
            {
                var ball = new Ball()
                {
                    Id = (string) obj["Id"],
                    X = (double) obj["X"],
                    Y = (double) obj["Y"],
                    Radius = (double) obj["R"],
                    Mass = (double) obj["M"],
                    SpeedX = (double) obj["SX"],
                    SpeedY = (double) obj["SY"],
                    MergeTimer = obj["TTF"] == null ? 0 : (int) obj["TTF"],
                };
                newBalls.Add(ball);
            }
            foreach (var newBall in newBalls)
            {
                var found = false;
                foreach (var memoryBall in Balls)
                    if (memoryBall.Id == newBall.Id)
                    {
                        found = true;
                        memoryBall.X = newBall.X;
                        memoryBall.Y = newBall.Y;
                        memoryBall.Radius = newBall.Radius;
                        memoryBall.Mass = newBall.Mass;
                        memoryBall.SpeedX = newBall.SpeedX;
                        memoryBall.SpeedY = newBall.SpeedY;
                        memoryBall.MergeTimer = newBall.MergeTimer;
                        continue;
                    }
                if (!found)
                    Balls.Add(newBall);
            }

            foreach (var memoryBall in new List<Ball>(Balls))
            {
                var isAlive = false;
                foreach (var newBall in newBalls)
                    if (memoryBall.Id == newBall.Id)
                        isAlive = true;
                if (!isAlive)
                    Balls.Remove(memoryBall);

            }
        }

        public static List<Point> FindVisibleFood(JArray objects)
        {
            var foodList = new List<Point>();
            foreach (JObject obj in objects)
            {
                var type = (String)obj.GetValue("T");
                if (type.Equals("F"))
                {
                    foodList.Add(new Point((double)obj.GetValue("X"), (double)obj.GetValue("Y")));
                }
            }
            return foodList;
        }

        public static List<Ball> FindEnemyBalls(JArray objects)
        {
            var enemyBallsList = new List<Ball>();
            foreach (JObject obj in objects)
            {
                var type = (String)obj.GetValue("T");
                if (type.Equals("P"))
                {
                    var enemyball = new Ball()
                    {
                        Id = (string)obj["Id"],
                        X = (double)obj["X"],
                        Y = (double)obj["Y"],
                        Radius = (double)obj["R"],
                        Mass = (double)obj["M"],
                    };
                    enemyBallsList.Add(enemyball);

                }
            }
            return enemyBallsList;
        }

    }
}