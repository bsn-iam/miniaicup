﻿using System;

namespace Strategy
{
    public class BonusMap
    {
        public double[,] Table;

        public double[,] RealTable { get; internal set; }

        public MapType MapType { get; }
        public double Weight { get; set; }
        public bool IsEmpty { get; internal set; }
        public int XMapSize { get; }

        public int YMapSize { get; }
        public int XWorldSize { get; }
        public int YWorldSize { get; }
        public double MapCellWidth { get; }
        public Range2Int ActiveMapRange { get; internal set; }
        public Range2 ActiveWorldRange { get; internal set; }
        public long MapTime { get; private set; }
        public string Name { get; private set; }

        public BonusMap(int xWorldSize, int yWorldSize, int xMapSize, Range2 activeWorldRange, MapType mapType)
        {
            MapType = mapType;

            XMapSize = xMapSize;
            XWorldSize = xWorldSize;
            YWorldSize = yWorldSize;
            
            MapCellWidth = (double) XWorldSize / XMapSize; // > 1
            YMapSize = XMapSize * YWorldSize / XWorldSize;

            Table = new double[XMapSize + 1, YMapSize + 1];
            //RealTable = new double[XMapSize + 1, YMapSize + 1];

            ActiveWorldRange = new Range2( activeWorldRange.XMin,
                                           activeWorldRange.XMax,
                                           activeWorldRange.YMin,
                                           activeWorldRange.YMax);

            ActiveMapRange = new Range2Int( (int)Math.Round(activeWorldRange.XMin / MapCellWidth), 
                                            (int)Math.Round(activeWorldRange.XMax / MapCellWidth), 
                                            (int)Math.Round(activeWorldRange.YMin / MapCellWidth), 
                                            (int)Math.Round(activeWorldRange.YMax / MapCellWidth));

            if (ActiveMapRange.XMin > XMapSize ||
                ActiveMapRange.XMax > XMapSize ||
                ActiveMapRange.YMin > YMapSize ||
                ActiveMapRange.YMax > YMapSize)
                throw new Exception("Wrong ActiveRange values!");

        }


        public BonusMap(BonusMap templateMap, MapType mapType)
        {
            MapType = mapType;

            XMapSize = templateMap.XMapSize;
            XWorldSize = templateMap.XWorldSize;
            YWorldSize = templateMap.YWorldSize;

            MapCellWidth = (double) XWorldSize / XMapSize; // > 1
            YMapSize = XMapSize * YWorldSize / XWorldSize;

            Table = new double[XMapSize + 1, YMapSize + 1];
            //RealTable = new double[XMapSize + 1, YMapSize + 1];

            ActiveMapRange = new Range2Int(
                templateMap.ActiveMapRange.XMin,
                templateMap.ActiveMapRange.XMax,
                templateMap.ActiveMapRange.YMin,
                templateMap.ActiveMapRange.YMax);
            ActiveWorldRange = new Range2(
                templateMap.ActiveWorldRange.XMin,
                templateMap.ActiveWorldRange.XMax,
                templateMap.ActiveWorldRange.YMin,
                templateMap.ActiveWorldRange.YMax);

        }

        internal void ResetActiveRange()
        {
            ActiveMapRange = new Range2Int(0, XMapSize, 0, YMapSize);
        }

        public void Reflect()
        {
            for (int i = 0; i < XMapSize; i++)
                for (int j = 0; j < YMapSize; j++)
                    Table[i, j] = -Table[i, j];
        }

        public BonusMap Trim()
        {
            double maxValue = Double.MinValue;
            double minValue = Double.MaxValue;

            //find max value of the map
            var iMin = ActiveMapRange.XMin;
            var iMax = ActiveMapRange.XMax;
            var jMin = ActiveMapRange.YMin;
            var jMax = ActiveMapRange.YMax;

            for (int i = iMin; i < iMax; i++)
                for (int j = jMin; j < jMax; j++)
                { 
                    if (Table[i, j] > maxValue)
                        maxValue = Table[i, j];
                    if (Table[i, j] < minValue)
                        minValue = Table[i, j];
                }

            if (Math.Abs(minValue - maxValue) < Double.Epsilon)
            {
                IsEmpty = true;
                return this;
            }

            //scale map to range [0, 1]
            for (int i = iMin; i < iMax; i++)
                for (int j = jMin; j < jMax; j++)
                    Table[i, j] = (Table[i, j] - minValue) / (maxValue - minValue);

            return this;
        }

        internal BonusMap SetName(string name)
        {
            Name = name;
            return this;
        }

        internal BonusMap SetTime(long elapsedMilliseconds)
        {
            MapTime = elapsedMilliseconds;
            return this;
        }

        public void AddUnitCalculation(Point unitAbsolutePosition, double maxValueDistance, double maxValue, double zeroValueDistance)
        {
            if (maxValueDistance > zeroValueDistance)
            {
                throw new Exception("Wrong distance limits.");
                //zeroValueDistance = maxValueDistance;
            }

            var maxValueDistanceSquared = maxValueDistance * maxValueDistance;
            var zeroValueDistanceSquared = zeroValueDistance * zeroValueDistance;

            var power = 2;
            var maxValuePow = Math.Pow(maxValue, power);

            for (int i = ActiveMapRange.XMin; i < ActiveMapRange.XMax; i++)
                for (int j = ActiveMapRange.YMin; j < ActiveMapRange.YMax; j++)
                {
                    var distanceSquared = unitAbsolutePosition.GetSquaredDistanceTo(i * MapCellWidth, j * MapCellWidth);

                    if (distanceSquared <= maxValueDistanceSquared)
                    {
                        if (MapType == MapType.Flat)
                            Table[i, j] = Math.Max(maxValuePow, Table[i, j]);
                        else
                            Table[i, j] += maxValuePow;
                    }

                    if (distanceSquared > maxValueDistanceSquared && distanceSquared < zeroValueDistanceSquared)
                    {
                        var value = maxValue - ((distanceSquared - maxValueDistanceSquared) / zeroValueDistanceSquared);
                        var valuePow = Math.Pow(value, power);

                        if (MapType == MapType.Flat)
                        {
                            Table[i, j] = Math.Max(valuePow, Table[i, j]);
                        }
                        else
                        {
                            Table[i, j] += valuePow;
                        }

                    }
                }
        }


        public BonusMap SetWeight(double weigth)
        {
            Weight = weigth;
            return this;
        }

        public void SetRealTable()
        {
            RealTable = (double[,])Table.Clone();
        }

    }



    public interface IBonusMap
    {
        void GenerateMap(MapType type, int weigth, double range);
    }

    public enum MapType
    {
        Additive,
        Flat,
        Final,
    }
}

