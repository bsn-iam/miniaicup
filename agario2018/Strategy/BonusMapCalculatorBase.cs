﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Strategy
{
    public abstract class BonusMapCalculatorBase
    {
        public BonusMap CalculateResultingMap(List<BonusMap> bonusMapList)
        {
            var sumMap = new BonusMap(bonusMapList.First(), MapType.Final);
            //sumMap.ResetActiveRange();

            foreach (var map in bonusMapList)
            {
                map.Trim();

                if (map.IsEmpty)
                    continue;
                var iMin = map.ActiveMapRange.XMin;
                var iMax = map.ActiveMapRange.XMax;
                var jMin = map.ActiveMapRange.YMin;
                var jMax = map.ActiveMapRange.YMax;

                for (int i = iMin; i < iMax; i++)
                    for (int j = jMin; j < jMax; j++)
                        sumMap.Table[i, j] += map.Table[i, j] * map.Weight;
            }

//            CheckTileGeneration(bonusMapList, 8); //Debug only

            return sumMap.Trim();
            //return sumMap;
        }


        public static Point GetBestDestination(Dictionary<Point, double> possibleRays)
        {
            var maxRayWin = Double.MinValue;
            var chosenDestination = new Point();
            foreach (var ray in possibleRays)
                if (ray.Value > maxRayWin)
                {
                    maxRayWin = ray.Value;
                    chosenDestination = ray.Key;
                }
            return chosenDestination;
        }


        public static Dictionary<Point, double> GeneratePossibleRays(BonusMap map, Point initialPoint, double radius, int angleStep, double startAngleDeg = 0, double endAngleDeg = 360)
        {

            radius = Math.Max(radius, 3 * map.MapCellWidth);
            var possibleRays = new Dictionary<Point, double>();

            for (int angle = (int)Math.Min(-startAngleDeg, -endAngleDeg); angle < (int) Math.Max(-startAngleDeg, -endAngleDeg); angle += angleStep)
            {
                double angleSI = Math.PI / 180 * angle;
                var possibleDestination = new Point(initialPoint.X + radius * Math.Sin(angleSI),
                    initialPoint.Y + radius * Math.Cos(angleSI));
                if (possibleDestination.X < 0 || possibleDestination.Y < 0 || possibleDestination.X >= map.XWorldSize ||
                    possibleDestination.Y >= map.YWorldSize)
                    continue;

                var rayWeightValues = CalculateRayWeight(map, initialPoint, possibleDestination);

                if (rayWeightValues.Any())
                    possibleRays.Add(possibleDestination, rayWeightValues.Average());
            }
            return possibleRays;
        }

        private static List<double> CalculateRayWeight(BonusMap map, Point possibleDestination, Point initialPoint)
        {
            Point p1, p2;
            bool isXYInverted = false;

            if (Math.Abs(possibleDestination.X - initialPoint.X) > Math.Abs(possibleDestination.Y - initialPoint.Y))
            {
                p1 = new Point(possibleDestination.X, possibleDestination.Y);
                p2 = new Point(initialPoint.X, initialPoint.Y);
            }
            else
            {
                //x-y invertion
                p1 = new Point(possibleDestination.Y, possibleDestination.X);
                p2 = new Point(initialPoint.Y, initialPoint.X);
                isXYInverted = true;

            }
            // diff x > diff y

            if (p2.X < p1.X)
            {
                var temp = new Point(p1);
                p1 = new Point(p2);
                p2 = new Point(temp);
            }
            //p1.X is always less then p2.X
            //if (p1.X > p2.X)
            //    throw new Exception("Wrong calculation");

            var tanA = (p2.Y - p1.Y) / (p2.X - p1.X);

            var cellValuesOnRay = new List<double>();

            for (int i = 0; i < (p2.X - p1.X); i++)
            {
                var x = p2.X - i;
                var y = p1.Y + (x - p1.X) * tanA;
                var mapX = (int)Math.Round(x / map.MapCellWidth);
                var mapY = (int)Math.Round(y / map.MapCellWidth);
                if (isXYInverted)
                {
                    var tempX = mapX;
                    mapX = mapY;
                    mapY = tempX;
                }
                cellValuesOnRay.Add(map.Table[mapX, mapY]);

            }
            if (!cellValuesOnRay.Any())
                throw new Exception("Empty values on the ray.");
            return cellValuesOnRay;
        }

        private static void CheckTileGeneration(List<BonusMap> bonusMapList, int tileWidth)
        {
#if DEBUG
            foreach (var map in bonusMapList)
            {
                var tileGenerator=new TileGenerator();
                var tileList = tileGenerator.GetTileList(map);
            }
#endif
        }

        public static void SetRealTables(List<BonusMap> bonusMapList)
        {
#if DEBUG
            foreach (var map in bonusMapList)
                map.SetRealTable();
#endif
        }

        public BonusMap GenerateBorderMap(BonusMap map, double borderWidth, double circleRadius)
        {
            var greenWorldZoneX = map.XWorldSize - borderWidth;
            var greenWorldZoneY = map.YWorldSize - borderWidth;


            for (int i = map.ActiveMapRange.XMin; i < map.ActiveMapRange.XMax; i++)
                for (int j = map.ActiveMapRange.YMin; j < map.ActiveMapRange.YMax; j++)
                {
                var iWorld = i * map.MapCellWidth;
                var jWorld = j * map.MapCellWidth;

                    var dxdy = PointToBorderDistances(new Point(iWorld, jWorld), map.XWorldSize, map.YWorldSize);
                    var dx = dxdy["X"];
                    var dy = dxdy["Y"];

                    var circleDistance = circleRadius + borderWidth;

                    if (dx < circleDistance && dy < circleDistance)
                    { 
                        var cp = new Point(iWorld, jWorld);
                        var circle1 = new Point(circleDistance, circleDistance);
                        var circle2 = new Point(map.XWorldSize - circleDistance, circleDistance);
                        var circle3 = new Point(circleDistance, map.YWorldSize - circleDistance);
                        var circle4 = new Point(map.XWorldSize - circleDistance, map.YWorldSize - circleDistance);

                        var distDict = new List<double>();
                        distDict.Add((cp.X - circle1.X) * (cp.X - circle1.X) + (cp.Y - circle1.Y) * (cp.Y - circle1.Y));
                        distDict.Add((cp.X - circle2.X) * (cp.X - circle2.X) + (cp.Y - circle2.Y) * (cp.Y - circle2.Y));
                        distDict.Add((cp.X - circle3.X) * (cp.X - circle3.X) + (cp.Y - circle3.Y) * (cp.Y - circle3.Y));
                        distDict.Add((cp.X - circle4.X) * (cp.X - circle4.X) + (cp.Y - circle4.Y) * (cp.Y - circle4.Y));

                        var r = Math.Sqrt(distDict.Min());

                        if (r > circleRadius)
                        {
                            map.Table[i, j] = r - circleRadius;
                        }
                        
                    }
                    if (dx > circleRadius && dy < borderWidth)
                        map.Table[i, j] = borderWidth - dy;
                    if (dx < borderWidth && dy > circleRadius)
                        map.Table[i, j] = borderWidth - dx;
                }

            return map;
        }

        private Dictionary<string, double> PointToBorderDistances(Point point, int xWorldSize, int yWorldSize)
        {
            var dx = Math.Min(point.X, xWorldSize - point.X);
            var dy = Math.Min(point.Y, yWorldSize - point.Y);

            var dict = new Dictionary<string, double>
            {
                { "X", dx },
                { "Y", dy }
            };

            return dict;
        }
    }
}
