﻿namespace Strategy
{
    public class Constants
    {
        //ключи GAME_WIDTH и GAME_HEIGHT - размеры мира(int);
        //GAME_TICKS - длительность игры в тиках(int);
        //FOOD_MASS - масса еды(float) (меняется от 1.0 до 4.0);
        //MAX_FRAGS_CNT - максимальное количество фрагментов у одного игрока(int) (от 4 до 16);
        //TICKS_TIL_FUSION - кол-во тиков после деления или взрыва, когда можно слить фрагменты(int) (от 150 до 500);
        //VIRUS_RADIUS - радиус вируса(float) (от 15.0 до 40.0);
        //VIRUS_SPLIT_MASS - критическая масса, по достижению которой вирус поделится(float) (от 50.0 до 100.0)
        //VISCOSITY - вязкость среды, от которой зависит скорость замедления вирусов, выбросов и фрагментов после деления(float) (от 0.05 - наименьшая вязкость, до 0.5)
        //INERTION_FACTOR - параметр регулировки инерции(насколько быстро изменяется вектор скорости при смене направления) (float) (от 1.0 - наибольшая инерция, до 20.0)
        //SPEED_FACTOR - параметр регулировки максимальной скорости(float) (от 25.0 до 100.0 - космические скорости)

        public double GameWidth { get; set; }
        public double GameHeight { get; set; }
        public double FoodMass { get; set; }
        public int MaxFragsCount { get; set; }
        public int TicksTillFusion { get; set; }
        public double VirusRadius { get; set; }
        public double VirusSplitMass { get; set; }
        public double Viscosity { get; set; }
        public double InertionFactor { get; set; }
        public double SpeedFactor { get; set; }

        public int MapPointsAmount { get; } = 132;

    }
}
