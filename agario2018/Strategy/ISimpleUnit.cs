﻿namespace Strategy
{
    public interface ISimpleUnit
    {
        string Id { get; set; }
        double X { get; set; }
        double Y { get; set; }

        ISimpleUnit Clone();
    }
}