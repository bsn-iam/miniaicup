﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Strategy
{
    public class Universe
    {
        public List<Ball> Balls;
        public List<Ball> EnemyBalls;
        public SortedList<int, WorldState> WorldStateList;
        public long MainTimer;
        public int TickIndex;
        public List<Point> VisibleFoodPoints;

        public Universe(List<Ball> balls, List<Ball> enemyBalls, SortedList<int, WorldState> worldStateList, List<Point> visibleFoodPoints, long mainTimer, int tickIndex)
        {
            this.Balls = new List<Ball>(balls);
            this.EnemyBalls = new List<Ball>(enemyBalls);
            this.WorldStateList = new SortedList<int, WorldState>(worldStateList);
            this.VisibleFoodPoints = new List<Point>(visibleFoodPoints);
            this.MainTimer = mainTimer;
            this.TickIndex = tickIndex;
        }
    }

}