﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public class Route
    {
        private List<Point> pointList = new List<Point>();
        public List<Point> Points { get { return pointList; } }
        private List<double> UsedPoints = new List<double>();
        private Point StartPoint = new Point();
        private Point nextPoint = new Point();
        public Point NextPoint { get { return nextPoint; } }

        private BonusMap Map;

        public Route(BonusMap map, Point initialWorldPoint, int maxLength)
        {
            var mapCellWidth = map.MapCellWidth;
            var currentPoint = new Point(Math.Round(initialWorldPoint.X / mapCellWidth), Math.Round(initialWorldPoint.Y / mapCellWidth));
            pointList.Add(currentPoint);
            StartPoint = currentPoint.Clone();
            UsedPoints.Add(currentPoint.Hash);
            Map = map;

            int counter = 0;
            while (currentPoint.IsInRange(map.ActiveMapRange, false) && counter < maxLength)
            {
                Dictionary<Point, double> weigthsAround = GetPointsAround(map, currentPoint);
                if (!weigthsAround.Any())
                    break;
                var bestPoint = weigthsAround.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;

                if (map.Table[(int)currentPoint.X, (int)currentPoint.Y] >= map.Table[(int)bestPoint.X, (int)bestPoint.Y])
                    break;

                pointList.Add(bestPoint);
                UsedPoints.Add(bestPoint.Hash);
                currentPoint = bestPoint.Clone();
                counter++;
            }
        }


        private Dictionary<Point, double> GetPointsAround(BonusMap map, Point currentPoint)
        {
            var weigthsAround = new Dictionary<Point, double>();
            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(0, 1));
            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(1, 1));
            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(1, 0));

            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(0, -1));
            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(-1, -1));
            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(-1, 0));

            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(1, -1));
            weigthsAround = AddWeigthToDict(currentPoint, weigthsAround, new Point(-1, 1));

            return weigthsAround;
        }

        internal Point CalculateWorldDestination(double radius)
        {
            var mapPointId = 0;
            var radiusSq = radius * radius;
            for (var i = pointList.Count - 1; i >= 0; i--)
            {
                if (Math.Abs(pointList[i].XYSum - StartPoint.XYSum) < radius)
                {
                    var distSq = GetStartEndDistSq(i);
                    if (distSq < radiusSq)
                    {
                        mapPointId = i;
                        break;
                    }
                }
            }
            if (mapPointId == 0)
                return new Point(StartPoint.X * Map.MapCellWidth, StartPoint.Y * Map.MapCellWidth);

            var distDebug = (pointList[mapPointId] - StartPoint).Length;

            var resultPointId = 0;
            for (var i = mapPointId; i <= pointList.Count - 1; i++)
            {
                var distSq = GetStartEndDistSq(i);

                var ee = 0;
                if (i > 10)
                    ee = 0;

                if (distSq > radiusSq)
                {
                    resultPointId = i;
                    break;
                }

            }
            var mapPoint = pointList[resultPointId];
            nextPoint = mapPoint;
            return new Point(mapPoint.X * Map.MapCellWidth, mapPoint.Y * Map.MapCellWidth);
        }

        private double GetStartEndDistSq(int i)
        {
            var distVec = pointList[i] - StartPoint;
            var distSq = distVec.X * distVec.X + distVec.Y * distVec.Y;
            return distSq;
        }

        internal void ExtendTillRadius(double maxWorldStepPerTick)
        {
            if (pointList.Count < 2)
                return;
            var stepRange = new Range2(Math.Max(0, StartPoint.X - maxWorldStepPerTick),
                Math.Min(StartPoint.X + maxWorldStepPerTick, Map.XMapSize), 
                Math.Max(0, StartPoint.Y - maxWorldStepPerTick), 
                Math.Min(StartPoint.Y + maxWorldStepPerTick, Map.YMapSize));

            if (!pointList.Last().IsInRange(stepRange, false))
                return;

            int range = Math.Max(1, pointList.Count / 3);
            var dx = (pointList.Last().X - pointList[pointList.Count - 1 - range].X) / range;
            var dy = (pointList.Last().Y - pointList[pointList.Count - 1 - range].Y) / range;
            while (pointList.Last().IsInRange(stepRange, false))
                pointList.Add(new Point(pointList.Last().X + dx, pointList.Last().Y + dy));
        }

        private Dictionary<Point, double> AddWeigthToDict(Point currentPosition, Dictionary<Point, double> weigthsAround, Point shift)
        {
            var somePoint = currentPosition + shift;
            if (somePoint.IsInRange(Map.ActiveMapRange, false) && !UsedPoints.Contains(somePoint.Hash))
            {
                weigthsAround.Add(somePoint, Map.Table[(int)somePoint.X, (int)somePoint.Y]);
                //usedPoints.Add(somePoint.Hash);
            }
            return weigthsAround;
        }


    }
}
