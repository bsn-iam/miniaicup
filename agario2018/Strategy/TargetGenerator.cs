﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms.VisualStyles;
using Newtonsoft.Json.Linq;

namespace Strategy
{
    public class TargetGenerator
    {
        private static JObject Sprite = new JObject();
        private static double rotationAngle = 0;


        public JObject GenerateTarget(Ball ball, List<Point>visibleFoodPoints, List<Ball> enemyBalls, JArray objects, long mainElapsedTime)
        {
            var target = new Point();
            var message = "";
            var sprite = new JObject();

            if (visibleFoodPoints.Any() || enemyBalls.Any(eb => ball.Mass > 1.3 * eb.Mass || 1.1 * ball.Mass < eb.Mass))  //Duplication in BonusMapCalculator!
            {
                var mapCalculator = new BonusMapCalculator();
                ball.TargetPoint = mapCalculator.GenerateTargetPoint(ball, visibleFoodPoints, enemyBalls);

                var innertialTarget = GenerateInertialTarget(ball);

                target = ExtendTargetTillBorder(ball, innertialTarget);

                message = $"{ball.MapCalculationTime}/{mainElapsedTime}ms.[{ball.X:f0},{ball.Y:f0} --> {target.X:f0},{target.Y:f0}]";
                //sprite.Add(new JProperty("Id", ball.Id));
                //sprite.Add(new JProperty("S", $"[{target.X}-{target.Y}]"));

            }
            else
            {
                var width = Strategy.Constants.GameWidth;
                var height = Strategy.Constants.GameHeight;
                target.X = width / 3 * Math.Sin(rotationAngle) + width * 0.5;
                target.Y = height / 3 * Math.Cos(rotationAngle) + height * 0.5;
                target = ExtendTargetTillBorder(ball, target);
                rotationAngle += 0.02;
                message = "No food";
            }

            var split = CheckForSplit(ball);

            return GetResult(target, message, sprite, split);
        }

        private bool CheckForSplit(Ball ball)
        {
            if (ball.Mass > 120 && !Strategy.EnemyBalls.Any())
                return true;
            if (IsReadyToSplitAtack(ball))
                return true;

            return false;
        }

        private bool IsReadyToSplitAtack(Ball ball)
        {
            if (ball.Mass > 120 && Strategy.EnemyBalls.Any())
            {
                var nearestEnemy = FindNearestEnemyBall(ball);
                var deviation = Geometry.GetAngleBetween(new Point(ball.SpeedX, ball.SpeedY),
                    new Point(nearestEnemy.X - ball.X, nearestEnemy.Y - ball.Y)) * 180 / Math.PI; // Deviation from shot course in degrees

                if (0.5 * ball.Mass >= 1.25 * nearestEnemy.Mass && deviation < 15)
                    return true;
            }

            return false;
        }

        private static Ball FindNearestEnemyBall(Ball ball)
        {
            var enemyToMeDistance = new Dictionary<Ball, double>();
            foreach (var enemyBall in Strategy.EnemyBalls)
            {
                var squaredDistance = (enemyBall.X - ball.X) * (enemyBall.X - ball.X) + (enemyBall.Y - ball.Y) * (enemyBall.Y - ball.Y);
                enemyToMeDistance.Add(enemyBall, squaredDistance);
            }

            var minDistance = Double.MaxValue;
            var closestEnemyBall = enemyToMeDistance.First().Key;
            foreach (var ballDist in enemyToMeDistance)
            {
                if (ballDist.Value < minDistance)
                {
                    minDistance = ballDist.Value;
                    closestEnemyBall = ballDist.Key;
                }
            }
            return closestEnemyBall;
        }

        private Point GenerateInertialTarget(Ball ball)
        {
            //speed_x += (nx * max_speed - speed_x) * INERTION_FACTOR / mass;
            //speed_y += (ny * max_speed - speed_y) * INERTION_FACTOR / mass;

            var currentVector = new Point(ball.SpeedX, ball.SpeedY);
            var targetVector = new Point(ball.TargetPoint.X - ball.X, ball.TargetPoint.Y - ball.Y);
            var orderVector = targetVector.Clone();

            var orderLength = orderVector.Length; //Order length

            if (Math.Abs(orderLength) < 0.01)
                return ball.TargetPoint;

            var inertionKoeff = Strategy.Constants.InertionFactor / ball.Mass;
            var maxSpeed = Strategy.Constants.SpeedFactor / Math.Sqrt(ball.Mass); //max step
            var dsx = (orderVector.X / orderLength * maxSpeed - currentVector.X) * inertionKoeff;
            var dsy = (orderVector.Y / orderLength * maxSpeed - currentVector.Y) * inertionKoeff;

            var accuracy = 0.75;//0.6; //max correction for orientation/target angle

            var targetOrientationAngle = Geometry.GetAngleBetween(orderVector, currentVector) * 180 / Math.PI; // in degrees
            ball.TargetPointOrientationAngle = targetOrientationAngle;
            if (targetOrientationAngle < 10)
                return RelativeVectorToPoint(ball, orderVector);

            var resultVector = new Point(dsx, dsy) + currentVector;
            var targetResultAngle = Geometry.GetAngleBetween(targetVector, resultVector) * 180 / Math.PI; // in degrees
            ball.TargetResultAngle = targetResultAngle;

            var dx = GetXIncrementValue(resultVector, targetVector);

            var gameRange = new Range2(0 + 2, Strategy.Constants.GameWidth - 2, 0 + 2,
                Strategy.Constants.GameHeight - 2);
            var initialTargetResultAngle = targetResultAngle;

            while (targetResultAngle > targetOrientationAngle * (1 - accuracy) &&
                    targetResultAngle <= initialTargetResultAngle &&
                    RelativeVectorToPoint(ball, orderVector).IsInRange(gameRange))
            {
                orderVector.X += dx;
                dsx = (orderVector.X / orderLength * maxSpeed - currentVector.X) * inertionKoeff;
                resultVector = new Point(dsx, dsy) + currentVector;
                targetResultAngle = Geometry.GetAngleBetween(targetVector, resultVector) * 180 / Math.PI; // in degrees
            }

            ball.InertialOrder = RelativeVectorToPoint(ball, orderVector);
            ball.Prediction = RelativeVectorToPoint(ball, resultVector);
            return ball.InertialOrder;
        }

        private static Point RelativeVectorToPoint(Ball ball, Point vector)
        {
            var currentPosition = new Point(ball.X, ball.Y);
            return currentPosition + vector;

        }

        private static int GetXIncrementValue(Point resultVector, Point targetVector)
        {
            var resultXZeroAngle = Geometry.GetAngleBetween(new Point(1, 0), resultVector);
            var targetXZeroAngle = Geometry.GetAngleBetween(new Point(1, 0), targetVector);
            var condition = (resultXZeroAngle - targetXZeroAngle) * resultVector.X * targetVector.X;
            var dx = condition < 0 ? 1 : -1;
            return dx;
        }


        private static Point ExtendTargetTillBorder(Ball ball, Point target)
        {
            if (Math.Abs(target.X - ball.X) <= 0.1)
                return target;

            if (Strategy.Balls.Count == 1)
                return target;

            var borderTarget = target;
            var dx = target.X - ball.X > 0 ? 1 : -1;
            var yxRatio = (target.Y - ball.Y) / (target.X - ball.X);
            var dy = dx * yxRatio;

            var worldRange = new Range2(0, Strategy.Constants.GameWidth, 0, Strategy.Constants.GameHeight);

            while (borderTarget.IsInRange(worldRange))
            {
                borderTarget.X += dx;
                borderTarget.Y += dy;
            }
            borderTarget.X -= dx;
            borderTarget.Y -= dy;

            if (!borderTarget.IsInRange(worldRange))
                throw new Exception("Wrong calcluation of border target point.");

            return borderTarget;
        }

        private static JObject GetResult(Point target, string message, JObject newSprite, bool split)
        {
            // выполнить деление, выброс
            // "Split": true,
            // "Eject": true,

            Sprite = newSprite ?? Sprite;
            var result = new JObject
            {
                ["X"] = target.X,
                ["Y"] = target.Y,
                ["Debug"] = message,
                ["Split"] = split,
                ["Sprite"] = Sprite.ToString()
            };
            return result;
        }


    }
}